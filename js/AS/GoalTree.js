AS.GoalTree = function(knowledge) {
    this.K = knowledge;

    this._backtracking = false;

    // tree of possible directions to goal
    this.tree = new AS.GoalNode(this, this.K.position, null);
    this.currentNode = this.tree;

    // make first set of movement options
    this.currentNode.makeChildren();
};

AS.GoalTree.prototype = {
    constructor: AS.GoalTree,

    get isBacktracking() {
        return this._backtracking;
    },

    getOptions: function() {
        this.backtracking = false;
        var currentOptions = this.filterUseful(this.filterUnvisited(
            this.currentNode.getOptions()));

        if (currentOptions.length > 0)
            return currentOptions;

        // backtrack
        this._backtracking = true;
        var backtrack = null;

/*
            currentOptions = this.filterUnvisited(this.currentNode.getOptions());
            if (currentOptions.length > 0)
                break;
        }*/
        if (this.currentNode.parent != null)
            return [this.currentNode.parent.position];
    },

    chooseOption: function(position) {
        // chose a backtracking option
        if (this.isBacktracking && this.currentNode.parent!= null &&
            AS.SpatialHelper.vectors3Equal(this.currentNode.parent.position, position)) {
            if (this.currentNode.parent != null) {
                this.currentNode = this.currentNode.parent;
            }
            else {
                AS.Log.append('Error', 'Unable to find any unvisited space. Possible loop?');
                throw new Error("Unable to find any unvisited space.");
            }
        }
        /*if (position.constructor === Array) {
            for (var b = 0; b < position.length; b++) {
                this.currentNode = this.currentNode.parent;
            }
        }*/
        else {
            var child = this.currentNode.getChildByPosition(position);
            this.currentNode = child;
        }
    },

    /**
     * Takes a list of positions and only returns those that have not been
     * visited.
     */
    filterUnvisited: function(positions) {
        var self = this;
        var filtered = [];
        $.each(positions, function(pi, position) {
            var flag = self.K.terrain.getFlag(position.x, position.y, position.z,
                'visited');
            if (flag == undefined || flag === false)
                filtered.push(position);
        });
        return filtered;
    },

    filterUseful: function(positions) {
        var self = this;
        var filtered = [];
        var useless = self.K.terrain.getFlag(self.K.position.x,
            self.K.position.y, self.K.position.z, 'useless');

        // if there are no useless movements from here, all is good
        if (useless == undefined)
            return positions;

        // otherwise prune the useless ones
        $.each(positions, function(pi, position) {
            var dir = AS.SpatialHelper.getDirectionForRelativePosition(position, self.K.position);
            if (!AS.SpatialHelper.vectors3Equal(dir, useless.from)) {
                filtered.push(position);
            }
        });
        return filtered;
    },
};

AS.GoalNode = function(tree, position, parent, options) {
    this.tree = tree;
    this.K = this.tree.K;

    this.position = position;

    this.parent = parent;
    this.children = [];

    options = options || {};
};

AS.GoalNode.prototype = {
    constructor: AS.GoalNode,

    isRoot: function() {
        return this.parent == null;
    },

    getOptions: function() {
        var options = [];

        if (this.children.length < 1)
            this.makeChildren();

        $.each(this.children, function(ci, child) {
            options.push(child.position);
        });
        return options;
    },

    getChildren: function() {
        return this.children;
    },

    getChildByPosition: function(position) {
        var gotChild = null;
        $.each(this.children, function(ci, child) {
            if (child.position.x == position.x && child.position.y == position.y
                && child.position.z == position.z) {
                gotChild = child;
            }
        });
        return gotChild;
    },

    addChildren: function(children) {
        var self = this;
        $.each(children, function(ci, child) {
            self.addChild(child);
        });
    },

    addChild: function(child) {
        this.children.push(child);
    },

    makeChildren: function() {
        var nextPositions = this.onlyPossible(this.eliminateParentFromMovements(
            this.movementsFromSpace()
        ));

        var self = this;
        $.each(nextPositions, function(ni, next) {
            self.addChild(new AS.GoalNode(self.tree, next, self));
        });
    },

    movementsFromSpace: function() {
        var pos = this.position;
        return [
            // posloženo prema preferenciji kretanja

            new THREE.Vector3(pos.x, pos.y, pos.z + 1),
            new THREE.Vector3(pos.x, pos.y + 1, pos.z),
            new THREE.Vector3(pos.x + 1, pos.y, pos.z),
            new THREE.Vector3(pos.x - 1, pos.y, pos.z),
            new THREE.Vector3(pos.x, pos.y - 1, pos.z),
            new THREE.Vector3(pos.x, pos.y, pos.z - 1),
        ];
    },

    onlyPossible: function(movements) {
        var maxdepth = this.K.maxdepth;
        var terrain = this.K.terrain;

        var possible = [];

        $.each(movements, function(mi, movement) {

            var flagOccupied = terrain.getOccupied(movement.x, movement.y,
                movement.z);
            var flagIntended = terrain.isIntended(movement.x, movement.y,
                movement.z);

            // cannot move to an occupied space or one that has an intent on it
            if (flagOccupied || flagIntended)
                return;

            var flagBlock = terrain.getFlag(movement.x, movement.y,
                movement.z, 'block') === true;
            var flagIntent = terrain.getFlag(movement.x, movement.y,
                movement.z, 'intent') != undefined;

            if (movement.z <= maxdepth && movement.z >= 0 &&
                movement.x >= 0 && movement.y >= 0 &&
                !flagIntent) {
                // it's possible
                possible.push(movement);
            }
        });

        return possible;
    },

    eliminateParentFromMovements: function(movements) {
        if (this.parent == null) {
            return movements;
        }
        var self = this;
        var possible = [];

        $.each(movements, function(mi, movement) {
            if (self.parent.position.x != movement.x ||
                self.parent.position.y != movement.y ||
                self.parent.position.z != movement.z) {
                possible.push(movement);
            }
        });
        return possible;
    },
};
