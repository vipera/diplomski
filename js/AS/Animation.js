/**
 * Generic animation object
 */
AS.Animation = function() {

};

AS.Animation.prototype = {
    constructor: AS.Animation,

    start: function() {},
    update: function() {},
    onUpdate: function(f) {},
    chain: function(animation) {},
};

/**
 * Tween animations implemented with Tween.js
 */
AS.Animation.Tween = function(object, to, time, delay) {
    this.tween = new TWEEN.Tween(object)
        .to(to, time);

    if (delay)
        this.tween.delay(delay);

    AS.Animation.call(this);
};

AS.Animation.Tween.prototype = AS.inherit(AS.Animation, {
	constructor: AS.Animation.Tween,

    start: function() {
        this.tween.start();
        AS.Animation.prototype.start.call(this);
    },

    update: function() {
        this.tween.update();
        AS.Animation.prototype.update.call(this);
    },

    chain: function(animation) {
        this.tween.chain(animation.tween);
    },

    onUpdate: function(f) {
        this.tween.onUpdate(f);
        AS.Animation.prototype.onUpdate.call(this, f);
    },

    onStart: function(f) {
        this.tween.onStart(f);
    },

    onComplete: function(f) {
        this.tween.onComplete(f);
    },

    interpolationType: function(type) {
        this.tween.interpolation(type);
    },

    easingType: function(type) {
        this.tween.easing(type);
    },
});
