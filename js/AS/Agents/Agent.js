AS.Agent = function(behaviour) {
    this.AID = 0; // agent ID
    this.agentname = this.agentname || 'Unknown agent';

    // generic "next-action" object
    // action can be anything a subclass understands - movement, messaging, etc.
    this.nextAction = AS.Agent.Actions.NoAction;
    this.nextActionState = {
        performed: false,
        performing: false,
    };

    // whether messaging is enabled
    this.messagingEnabled = true;
    this.receivedMessages = [];

    this.behaviour = behaviour || null;
    this.behaviour.setAgent(this);

    // knowledge
    this.K = {
        terrain: null,
        intentValidity: 1000, // 1 second
        goal: null,
    };

    // this call registers the agent with the environment
    this.register(this);
};
AS.Agent.Actions = {
    NoAction: 1000,

    Communication: {
        SendMessage: 1001,
    },
};
AS.Agent.Actions.prototype = {};

AS.Agent.prototype = {
    constructor: AS.Agent,

    get name() { return "A{0} ({1})".format(this.AID, this.agentname); },

    register: function() {
        ENV.registerAgent(this);
    },

    initKnowledge: function() {
        this.K.terrain = new AS.Terrain(this.K);
    },

    repeat: function() {
        // first check goal function
        if (this.K.goal) {
            if (this.K.goal(this)) {
                // Goal fulfilled!
                this.goalFulfilled();
                ENV.informAboutGoalFulfilment(this.AID);
                return;
            }
        }

        this.processMessages();
        this.perceiveSurroundings();
        this.chooseNextAction();
        this.performAction();
    },

    processMessages: function() {
        this.behaviour.processMessages();
    },

    perceiveSurroundings: function() {
        if (this.K.terrain)
            this.K.terrain.tick();
    },

    chooseNextAction: function() {
        // don't choose another action if previous one hasn't been performed
        if (!this.nextActionState.performed)
            return;

        this.nextAction = this.behaviour.decideAction();
        this.nextActionState = { performed: false, performing: false };
    },

    goalFulfilled: function() {
        this.behaviour.goalFulfilled();
    },

    // hypertimer tick - this is when the agent decides its actions
    tick: function() {
        this.repeat();
    },

    /**
     * Gives an agent a piece of information to immediately know. Should not be
     * called except when setting up.
     */
    inform: function(ontology, value) {
        this.K[ontology] = value;
    },

    performAction: function() {
        if (!this.nextAction || this.nextActionState.performing)
            return true;

        if (this.nextActionState.performed) {
            this.nextAction = null;
            return true;
        }

        switch (this.nextAction) {
            case AS.Agent.Actions.NoAction:
                this.nextActionState.performed = true;
                this.nextActionState.performing = false;

                return true;
        }
    },

    // update own graphics - called with each animation frame
    updateAnimation: function() {

    },

    knowsAction: function(level, action) {
        return action >= level && action < level + 1000;
    },

    // messaging functions for exchanging messages with other agents -----------
    sendMessage: function(to, ontology, message) {
        if (!this.messagingEnabled)
            return;
        ENV.message(this, to, ontology, message);
    },

    sendBroadcast: function(ontology, message) {
        if (!this.messagingEnabled)
            return;
        ENV.broadcast(this, ontology, message);
    },

    receiveMessage: function(from, ontology, message) {
        if (!this.messagingEnabled)
            return;
        this.receivedMessages.push({
            from: from,
            ontology: ontology,
            content: message,
            receivedTime: Date.now(),
        });
    },
};
