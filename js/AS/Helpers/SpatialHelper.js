AS.SpatialHelper = {
    UpVector: new THREE.Vector3(0, 1, 0),
    DownVector: new THREE.Vector3(0, -1, 0),
    XVector: new THREE.Vector3(1, 0, 0),
    NegXVector: new THREE.Vector3(-1, 0, 0),
    ZVector: new THREE.Vector3(0, 0, 1),
    NegZVector: new THREE.Vector3(0, 0, -1),

    get allDirectionVectors() {
        return [
            this.XVector,
            this.NegXVector,
            this.UpVector,
            this.DownVector,
            this.ZVector,
            this.NegZVector
        ];
    },

    vectors2Equal: function(vec1, vec2) {
        return vec1.x == vec2.x && vec1.y == vec2.y;
    },

    vectors3Equal: function(vec1, vec2) {
        return vec1.x == vec2.x && vec1.y == vec2.y && vec1.z == vec2.z;
    },

    nextRelativePositionInDirection: function(position, direction) {
        return position.clone().add(new THREE.Vector3(
            direction.x, direction.z, -direction.y
        ));
    },

    validRelativePosition: function(position) {
        if (position.x < 0 || position.y < 0 || position.z < 0 ||
            position.z > SIM.scenarioData.maxdepth) {
            return false;
        }
        return true;
    },

    getDirectionForRelativePosition: function(targetPosition, currentPosition) {
        var directionRelative = targetPosition.clone().sub(currentPosition);
        return new THREE.Vector3(
            directionRelative.x,
            -directionRelative.z,
            directionRelative.y
        );
    },

    /**
     * Checks whether a tight squeeze exists at a specific relative location.
     * A tight squeeze is a space surrounded by block cells on all sides except
     * two opposite ones.
     * @param {Vector3} Relative position
     * @return {Bool} Whether a tight squeeze exists at specified location
     */
    isTightSqueeze: function(position, terrain) {
        var offsets = [
            [ this.XVector, this.NegXVector, this.ZVector, this.NegZVector ],
            [ this.XVector, this.NegXVector, this.UpVector, this.DownVector ],
            [ this.UpVector, this.DownVector, this.ZVector, this.NegZVector ],
        ];

        var squeeze = false;
        $.each(offsets, function(osi, offsetCollection) {
            var squeezeSide = true;
            $.each(offsetCollection, function(oi, offset) {
                var pos = position.clone().add(offset);
                var flag = terrain.getFlag(pos.x, pos.y, pos.z, 'block');
                if (flag == undefined || flag == false) {
                    squeezeSide = false;
                    return true;
                }
            });

            squeeze = squeeze || squeezeSide;
            if (squeezeSide)
                return true;
        });
        return squeeze;
    },

    movementTranslationTable: function(compositeMovement) {
        if (!this.mtt) {
            this.mtt = {};

            // Up -----
            this.mtt[AS.Agent3D.Actions.Movement.UpForward] = [
                AS.Agent3D.Actions.Movement.Up,
                AS.Agent3D.Actions.Movement.Forward
            ];
            this.mtt[AS.Agent3D.Actions.Movement.UpForwardLeft] = [
                AS.Agent3D.Actions.Movement.Up,
                AS.Agent3D.Actions.Movement.Forward,
                AS.Agent3D.Actions.Movement.Left
            ];
            this.mtt[AS.Agent3D.Actions.Movement.UpForwardRight] = [
                AS.Agent3D.Actions.Movement.Up,
                AS.Agent3D.Actions.Movement.Forward,
                AS.Agent3D.Actions.Movement.Right
            ];
            this.mtt[AS.Agent3D.Actions.Movement.UpLeft] = [
                AS.Agent3D.Actions.Movement.Up,
                AS.Agent3D.Actions.Movement.Left
            ];
            this.mtt[AS.Agent3D.Actions.Movement.UpRight] = [
                AS.Agent3D.Actions.Movement.Up,
                AS.Agent3D.Actions.Movement.Right
            ];
            this.mtt[AS.Agent3D.Actions.Movement.UpBack] = [
                AS.Agent3D.Actions.Movement.Up,
                AS.Agent3D.Actions.Movement.Backward
            ];
            this.mtt[AS.Agent3D.Actions.Movement.UpBackLeft] = [
                AS.Agent3D.Actions.Movement.Up,
                AS.Agent3D.Actions.Movement.Backward,
                AS.Agent3D.Actions.Movement.Left
            ];
            this.mtt[AS.Agent3D.Actions.Movement.UpBackRight] = [
                AS.Agent3D.Actions.Movement.Up,
                AS.Agent3D.Actions.Movement.Backward,
                AS.Agent3D.Actions.Movement.Right
            ];

            // Same y-value -----
            this.mtt[AS.Agent3D.Actions.Movement.ForwardLeft] = [
                AS.Agent3D.Actions.Movement.Forward,
                AS.Agent3D.Actions.Movement.Left
            ];
            this.mtt[AS.Agent3D.Actions.Movement.ForwardRight] = [
                AS.Agent3D.Actions.Movement.Forward,
                AS.Agent3D.Actions.Movement.Right
            ];
            this.mtt[AS.Agent3D.Actions.Movement.BackwardLeft] = [
                AS.Agent3D.Actions.Movement.Backward,
                AS.Agent3D.Actions.Movement.Left
            ];
            this.mtt[AS.Agent3D.Actions.Movement.BackwardRight] = [
                AS.Agent3D.Actions.Movement.Backward,
                AS.Agent3D.Actions.Movement.Right
            ];

            // Down -----
            this.mtt[AS.Agent3D.Actions.Movement.DownForward] = [
                AS.Agent3D.Actions.Movement.Down,
                AS.Agent3D.Actions.Movement.Forward
            ];
            this.mtt[AS.Agent3D.Actions.Movement.DownForwardLeft] = [
                AS.Agent3D.Actions.Movement.Down,
                AS.Agent3D.Actions.Movement.Forward,
                AS.Agent3D.Actions.Movement.Left
            ];
            this.mtt[AS.Agent3D.Actions.Movement.DownForwardRight] = [
                AS.Agent3D.Actions.Movement.Down,
                AS.Agent3D.Actions.Movement.Forward,
                AS.Agent3D.Actions.Movement.Right
            ];
            this.mtt[AS.Agent3D.Actions.Movement.DownLeft] = [
                AS.Agent3D.Actions.Movement.Down,
                AS.Agent3D.Actions.Movement.Left
            ];
            this.mtt[AS.Agent3D.Actions.Movement.DownRight] = [
                AS.Agent3D.Actions.Movement.Down,
                AS.Agent3D.Actions.Movement.Right
            ];
            this.mtt[AS.Agent3D.Actions.Movement.DownBack] = [
                AS.Agent3D.Actions.Movement.Down,
                AS.Agent3D.Actions.Movement.Backward
            ];
            this.mtt[AS.Agent3D.Actions.Movement.DownBackLeft] = [
                AS.Agent3D.Actions.Movement.Down,
                AS.Agent3D.Actions.Movement.Backward,
                AS.Agent3D.Actions.Movement.Left
            ];
            this.mtt[AS.Agent3D.Actions.Movement.DownBackRight] = [
                AS.Agent3D.Actions.Movement.Down,
                AS.Agent3D.Actions.Movement.Backward,
                AS.Agent3D.Actions.Movement.Right
            ];
        }

        return this.mtt[compositeMovement];
    },

    getNextRelativePosition: function(agent, movement) {
        /*var offset = this.getSimpleMovementOffset(agent.position, agent.direction, movement, agent.unitdistance);
        offset.divideScalar(agent.unitdistance);*/
        var direction = new THREE.Vector3(agent.K.direction.x, 0, agent.K.direction.y);

        switch (movement) {
            case AS.Agent3D.Actions.Movement.Up:
                direction = this.UpVector;
                break;
            case AS.Agent3D.Actions.Movement.Down:
                direction = this.DownVector;
                break;
            case AS.Agent3D.Actions.Movement.Left:
                direction.cross(this.UpVector).multiplyScalar(-1);
                break;
            case AS.Agent3D.Actions.Movement.Right:
                direction.cross(this.UpVector);
                break;
            case AS.Agent3D.Actions.Movement.Backward:
                direction.multiplyScalar(-1);
                break;
        }
        var pos = this.nextRelativePositionInDirection(agent.K.position, direction);

        /*return agent.K.position.clone()
            .add(new THREE.Vector3(offset.x, offset.z, offset.y * -1));*/
        return pos;
    },

    getNextRelativeDirection: function(agent, rotation) {
        var rotationVec = new THREE.Vector3(
            agent.K.direction.x,
            agent.K.direction.y,
            0
        );
        var up = new THREE.Vector3(0, 0, 1);

        switch (rotation) {
            case AS.Agent3D.Actions.Rotation.Left:
                rotationVec.cross(up);
                break;
            case AS.Agent3D.Actions.Rotation.Right:
                rotationVec.cross(up).multiplyScalar(-1);
                break;
            case AS.Agent3D.Actions.Rotation.About:
                rotationVec.multiplyScalar(-1);
                break;
        }
        return new THREE.Vector2(rotationVec.x, rotationVec.y);
    },

    relativePositionToWorldPosition: function(position) {
        var unitlength = ENV.getUnitDistance();
        var center = ENV.getCenter();

        var world = new THREE.Vector3(
            unitlength / 2,
            -unitlength / 2,
            unitlength / 2
        );

        world.x += (position.x - center[0]) * unitlength;
        world.y -= position.z * unitlength;
        world.z += (position.y - center[1]) * unitlength;

        return world;
    },

    relativeDirectionToWorldDirection: function(direction) {
        return new THREE.Vector3(
            direction.x,
            0,
            direction.y
        );
    },

    worldDirectionToMovementAction: function(direction, agentDirection) {
        if (direction.y == 1)
            return AS.Agent3D.Actions.Movement.Up;
        if (direction.y == -1)
            return AS.Agent3D.Actions.Movement.Down;

        var agentDir3 = new THREE.Vector3(agentDirection.x, 0, agentDirection.y);

        // forward/backward
        if (this.vectors3Equal(direction, agentDir3))
            return AS.Agent3D.Actions.Movement.Forward;
        if (this.vectors3Equal(direction.clone().multiplyScalar(-1), agentDir3))
            return AS.Agent3D.Actions.Movement.Backward;

        // left/right
        if (this.vectors3Equal(direction.clone().cross(this.UpVector), agentDir3))
            return AS.Agent3D.Actions.Movement.Left;
        if (this.vectors3Equal(direction.clone().cross(this.UpVector).multiplyScalar(-1), agentDir3))
            return AS.Agent3D.Actions.Movement.Right;

        // error in other cases
        throw new Error('Direction is incorrect');
    },


    getSimpleMovementOffset: function(position, direction, movement, unitdistance) {
        var right = direction.clone().cross(new THREE.Vector3(0, 1, 0));
        var offset = new THREE.Vector3(0, 0, 0);

        switch (movement) {
            case AS.Agent3D.Actions.Movement.Forward:
                offset.add(direction.clone().multiplyScalar(unitdistance));
                break;

            case AS.Agent3D.Actions.Movement.Backward:
                offset.sub(direction.clone().multiplyScalar(unitdistance));
                break;

            case AS.Agent3D.Actions.Movement.Left:
                offset.sub(right.clone().multiplyScalar(unitdistance));
                break;

            case AS.Agent3D.Actions.Movement.Right:
                offset.add(right.clone().multiplyScalar(unitdistance));
                break;

            case AS.Agent3D.Actions.Movement.Up:
                offset.y += unitdistance;
                break;

            case AS.Agent3D.Actions.Movement.Down:
                offset.y -= unitdistance;
                break;
        }
        return offset;
    },

    getTweenObjectForMovement: function(position, direction, movement, unitdistance) {
        var self = this;
        var go = {};
        var additional = { x: null, y: null, z: null };

        // start with  the end where the object currently is
        var end = new THREE.Vector3(position.x, position.y, position.z);

        switch (movement) {
            case AS.Agent3D.Actions.Movement.Forward:
            case AS.Agent3D.Actions.Movement.Backward:
            case AS.Agent3D.Actions.Movement.Left:
            case AS.Agent3D.Actions.Movement.Right:
            case AS.Agent3D.Actions.Movement.Up:
            case AS.Agent3D.Actions.Movement.Down:
                end.add(this.getSimpleMovementOffset(position, direction,
                    movement, unitdistance));
                break;

            default: // all other movements are composite
                var movements = this.movementTranslationTable(movement);
                $.each(movements, function(index, mov) {
                    var offset = self.getSimpleMovementOffset(end,
                        direction, mov, unitdistance);
                    end.add(offset);
                });
                break;
        }

        go.x = additional.x ? [end.x, additional.x] : end.x;
        go.y = additional.y ? [end.y, additional.y] : end.y;
        go.z = additional.z ? [end.z, additional.z] : end.z;

        return {
            to: go,
            interpolation: additional.x ?
                TWEEN.Interpolation.Bezier : TWEEN.Interpolation.Linear
        };
    },
};
