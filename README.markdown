# Three.js agent behaviour simulator #

This is a part of a master's thesis on the behaviour of agents in 3D environments.

The included code is a browser-based 3D simulator of a cubic maze environment in
which agents can be deployed and their behaviours simulated.

## Description ##

The scenarios directory contains XML definitions of simulation scenarios that
conform to the included `agent-setup.xsd` file. Two scenarios are included that
demonstrate the basic capabilities of this tool.

The simulator itself depends on the provided PHP scripts generating a JSON
description of the scenario from the scenario XML files, thus this application
needs to be deployed as a Web application on a capable HTTP server.

The `js` directory includes both the logic of the simulator (`AS` directory) and
required libraries which I have included in the repo for ease of installation.

## Installation ##

### Prerequisites ###

The PHP scripts in this project require PHP 5.3+ with **libxml** support. Any
newer PHP installation should be able to run the scripts right off the bat.

### Installation instructions ###

  1. Get the source code from Git by downloading or cloning this repository.
  2. Place the whole contents of the project in a directory that can be served
  with a Web server configured with PHP support like Apache, nginx or what you
  want to use.

## License ##
This program is free software, licenced under the the permissive MIT licence.
Please see the COPYING file for additional information.
