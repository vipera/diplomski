AS.Submersible = function(behaviour) {
    // debug options
    this.debug = {
        showBoundingBox: true,
    };

    this.agentname = 'Submersible';

    this.geometries = {}; // this is where the various geometries are kept
    this.materials = {};
    this.matrices = {};

    AS.Agent3D.call(this, behaviour);

    this.init();

    this.cameras = this.cameras.concat([
        {
            id: 'camera-behind',
            name: 'Behind',
            camera: new THREE.PerspectiveCamera(
                SIM.cameraFov,
                160 / 90,
                SIM.cameraNear, SIM.cameraFar
            ),
            change: function(agent, camera) {
                camera.position.copy(agent.position.clone()
                    .sub(agent.direction.clone().multiplyScalar(2))
                );
                camera.lookAt(
                    camera.position.clone()
                        .add(agent.direction.clone().multiplyScalar(-1))
                );
            },
        },
        {
            id: 'camera-left',
            name: 'Left',
            camera: new THREE.PerspectiveCamera(
                SIM.cameraFov,
                160 / 90,
                SIM.cameraNear, SIM.cameraFar
            ),
            change: function(agent, camera) {
                camera.position.copy(agent.position.clone()
                    .sub(agent.direction.clone().cross(new THREE.Vector3(0, 1, 0)).multiplyScalar(4))
                );
                camera.lookAt(
                    camera.position.clone()
                        .add(agent.direction.clone().cross(new THREE.Vector3(0, 1, 0)).multiplyScalar(-1))
                );
            },
        },
        {
            id: 'camera-right',
            name: 'Right',
            camera: new THREE.PerspectiveCamera(
                SIM.cameraFov,
                160 / 90,
                SIM.cameraNear, SIM.cameraFar
            ),
            change: function(agent, camera) {
                camera.position.copy(agent.position.clone()
                    .sub(agent.direction.clone().cross(new THREE.Vector3(0, 1, 0)).multiplyScalar(-4))
                );
                camera.lookAt(
                    camera.position.clone()
                        .add(agent.direction.clone().cross(new THREE.Vector3(0, 1, 0)))
                );
            },
        },
        {
            id: 'camera-up',
            name: 'Up',
            camera: new THREE.PerspectiveCamera(
                SIM.cameraFov,
                160 / 90,
                SIM.cameraNear, SIM.cameraFar
            ),
            change: function(agent, camera) {
                camera.position.copy(agent.position.clone()
                    .add(new THREE.Vector3(0, 4, 0))
                );
                camera.up.copy(agent.direction.clone().multiplyScalar(-1));
                camera.lookAt(
                    camera.position.clone().add(new THREE.Vector3(0, 1, 0))
                );
            },
        },
        {
            id: 'camera-down',
            name: 'Down',
            camera: new THREE.PerspectiveCamera(
                SIM.cameraFov,
                160 / 90,
                SIM.cameraNear, SIM.cameraFar
            ),
            change: function(agent, camera) {
                camera.position.copy(agent.position);
                camera.up.copy(agent.direction);
                camera.lookAt(
                    camera.position.clone().add(new THREE.Vector3(0, -1, 0))
                );
            },
        },
    ]);
};

AS.Submersible.prototype = AS.inherit(AS.Agent3D, {
	constructor: AS.Submersible,

    init: function() {
        this.initGraphicalRepresentation();
    },

    initGraphicalRepresentation: function() {
        this.geometries.front = new THREE.SphereGeometry(
            3.4,
            20, 20, // segments - width, height
            0, Math.PI, // phi - start, length
            0, Math.PI // theta - start, length
        );
        this.geometries.front.scale(1, 1, 1.1);

        this.geometries.body = new THREE.CylinderGeometry(
            3.4,
            3.4,
            3.4, // height
            40, 1, // segments - radius, height
            true, // open ended
            0, Math.PI * 2 // theta - start, length
        );

        this.geometries.back = new THREE.SphereGeometry(
            3.4,
            20, 20, // segments - width, height
            0, Math.PI, // phi - start, length
            0, Math.PI // theta - start, length
        );
        this.geometries.back.scale(1, 1, 0.8);

        this.geometries.propeller = new THREE.TorusGeometry(
            1.3, // radius
            0.2, // tube size
            3, // radial segments
            150 // tubular segments
        );

        this.geometries.propellerblade = new THREE.BoxGeometry(
            0.1, 2.4, 0.2
        );

        this.geometries.camera = new THREE.BoxGeometry(0.5, 0.5, 1.2);

        this.makeGeometries();
        this.makeMaterials();

        this._meshes.main = new THREE.Mesh(this.geometries.submain, this.materials.yellowmetal);

        this._meshes.camera = new THREE.Mesh(this.geometries.camera,  this.materials.darkmetal);
        this._meshes.camera.applyMatrix(this.matrices.cameratrans);

        this._meshes.lprop = new THREE.Mesh(this.geometries.propellerblades, this.materials.darkmetal);
        this._meshes.lprop.applyMatrix(this.matrices.lproptrans);

        this._meshes.rprop = new THREE.Mesh(this.geometries.propellerblades, this.materials.darkmetal);
        this._meshes.rprop.applyMatrix(this.matrices.rproptrans)

        if (this.debug.showBoundingBox) {
            var bbox = new THREE.BoundingBoxHelper( this._meshes.main, 0xff0000 );
    		bbox.update();
    		this._meshes.mainbbox = bbox;
        }
    },

    makeGeometries: function() {
        var geometry = new THREE.Geometry();

        //geometry.merge(this.geometries['front']);
        geometry.merge(this.geometries.front);

        var matrix1 = new THREE.Matrix4()
            .makeTranslation(0, 0, -1.7);
        var matrix2 = new THREE.Matrix4()
            .makeRotationX(Math.PI / 2);

        geometry.merge(this.geometries.body, matrix1.multiply(matrix2));

        var matrix3 = new THREE.Matrix4()
            .makeTranslation(0, 0, 1.7);

        geometry.merge(this.geometries.back, matrix1.multiply(matrix2).multiply(matrix3));

        var matrix_leftprop = new THREE.Matrix4().makeTranslation(4, 0, -2.4);
        var matrix_rightprop = new THREE.Matrix4().makeTranslation(-4, 0, -2.4);
        this.matrices.lproptrans = matrix_leftprop;
        this.matrices.rproptrans = matrix_rightprop;
        geometry.merge(this.geometries.propeller, matrix_leftprop);
        geometry.merge(this.geometries.propeller, matrix_rightprop);

        this.matrices.cameratrans = new THREE.Matrix4().makeTranslation(0, 3.6, -0.4);
        //geometry.merge(this.geometries.camera, cameratrans);

        // propellers at 1/6 PI, 1/3 PI, 1/2 PI, 2/3 PI and 5/6 PI
        this.geometries.propellerblades = new THREE.Geometry();
        this.geometries.propellerblades.merge(this.geometries.propellerblade);
        this.geometries.propellerblades.merge(this.geometries.propellerblade, new THREE.Matrix4().makeRotationZ(Math.PI * 1/3));
        this.geometries.propellerblades.merge(this.geometries.propellerblade, new THREE.Matrix4().makeRotationZ(Math.PI * 2/3));
        this.geometries.propellerblades.merge(this.geometries.propellerblade, new THREE.Matrix4().makeRotationZ(Math.PI * 1/6));
        this.geometries.propellerblades.merge(this.geometries.propellerblade, new THREE.Matrix4().makeRotationZ(Math.PI * 1/2));
        this.geometries.propellerblades.merge(this.geometries.propellerblade, new THREE.Matrix4().makeRotationZ(Math.PI * 5/6));

        this.geometries.submain = geometry;

        this.geometries.lprop = new THREE.Geometry();
        this.geometries.rprop = new THREE.Geometry();
        this.geometries.lprop.merge(this.geometries.propellerblades, matrix_leftprop);
        this.geometries.rprop.merge(this.geometries.propellerblades, matrix_rightprop);
    },

    makeMaterials: function() {
        this.materials.yellowmetal = new THREE.MeshPhongMaterial({
			color: 0xf1e237,
			emissive: 0x343007,
			//side: THREE.DoubleSide,
			//shading: THREE.FlatShading,
			//transparent: true,
			//opacity: 0.8,
			//depthTest:true,
		});
        this.materials.darkmetal = new THREE.MeshPhongMaterial({
			color: 0x0d0d0d,
			emissive: 0x171714,
		});

        this.materials.orangemetal = new THREE.MeshPhongMaterial({
            color:0xbf4b22,
            emissive:0xae4f2e,
        });
    },

    updateAnimation: function() {
        //AS.MathHelper.rotateAroundObjectAxis(this._meshes.lprop, this.direction, 0.05);
        //AS.MathHelper.rotateAroundObjectAxis(this._meshes.rprop, this.direction, -0.05);

        // rotate propellers at constant rate
        this._meshes.lprop.rotateOnAxis(new THREE.Vector3(0,0,1), 0.05);
        this._meshes.rprop.rotateOnAxis(new THREE.Vector3(0,0,1), -0.05);

        // propagate call to agent3D
        AS.Agent3D.prototype.updateAnimation.call(this);
    },
});
