AS.Behaviours.Exploration = function() {
    this.className = "Exploration";
    this.description = "Explore terrain and find goal.";


    this.previousPosition = null;
    this.goalTree = null;
};

AS.Behaviours.Exploration.prototype = AS.inherit(AS.Behaviour, {
    constructor: AS.Behaviours.Exploration,

    buildGoalTree: function() {
        this.goalTree = new AS.GoalTree(this.agent.K);
    },

    /**
     * Picks the option that leads immediately closer to the goal, if the goal
     * is known. Otherwise, picks the first option in the list of possible ones.
     * @param {List} List of possible options (Vector3s)
     * @return {Vector3} Chosen option
     */
    pickOption: function(options) {
        var self = this;
        // return option that leads immediately closer to goal
        if (this.agent.K.knownGoal) {
            var option = options[0];
            var currentDist = AS.MathHelper.distance(option, this.agent.K.knownGoal);

            $.each(options, function(oi, opt) {
                var thisDist = AS.MathHelper.distance(opt, self.agent.K.knownGoal);
                if (thisDist < currentDist) {
                    option = opt;
                    currentDist = thisDist;
                }
            });
            return option;
        }
        // return first option
        return options[0];
    },

    decideAction: function() {
        if (this.goalTree == null)
            this.buildGoalTree();

        var self = this;

        // ask underlying class for action
        var baseAction = AS.Behaviour.prototype.decideAction.call(this);
        if (baseAction)
            return baseAction;

        // exploration algorithm

        var options = this.goalTree.getOptions();
        if (!options || options.length < 1)
            return AS.Agent.Actions.NoAction;

        // pick option (better algorithm preferred)
        var option = this.pickOption(options);
        var dir = AS.SpatialHelper.getDirectionForRelativePosition(option, this.agent.K.position);
        var movement = AS.SpatialHelper.worldDirectionToMovementAction(dir, this.agent.K.direction);

        // communicate that a path is "useless" if backtracking is necessary and
        // there is a single, single-cell entrance
        if (this.goalTree.isBacktracking && this.previousPosition) {

            if (AS.SpatialHelper.isTightSqueeze(this.previousPosition, this.agent.K.terrain))
            {
                var oppositeDir = dir.clone().multiplyScalar(-1);
                this.agent.sendBroadcast('USELESS', '{0},{1},{2} {3},{4},{5}'.format(
                    option.x, option.y, option.z,
                    oppositeDir.x, oppositeDir.y, oppositeDir.z
                ));
                AS.Log.append('Agent A{0}'.format(this.agent.AID),
                    'communicates that it is useless to go from ({0},{1},{2}) in direction ({3},{4},{5})'.format(
                    option.x, option.y, option.z,
                    oppositeDir.x, oppositeDir.y, oppositeDir.z
                ));
            }
        }

        // instead of moving left/right, rotate and go forward
        if (movement == AS.Agent3D.Actions.Movement.Left)
            return AS.Agent3D.Actions.Rotation.Left;
        if (movement == AS.Agent3D.Actions.Movement.Right)
            return AS.Agent3D.Actions.Rotation.Right;

        // now mark movement as chosen
        this.goalTree.chooseOption(option);
        this.agent.sendBroadcast('INTENT_MOVE', '{0},{1},{2} {3},{4},{5}'.format(
            this.agent.K.position.x, this.agent.K.position.y, this.agent.K.position.z,
            option.x, option.y, option.z
        ));

        this.previousPosition = this.agent.K.position;
        return movement;
    },

    processMessages: function() {
        // calling base method will remove those messages that the base class understands
        AS.Behaviour.prototype.processMessages.call(this);
    },

    goalFulfilled: function() {
        this.agent.sendBroadcast('GOAL_FOUND', '{0},{1},{2}'.format(
            this.agent.K.position.x,
            this.agent.K.position.y,
            this.agent.K.position.z
        ));
        AS.Behaviour.prototype.goalFulfilled.call(this);
    },
});
