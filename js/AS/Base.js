if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

// AS Definition
var AS = AS || {};

// merge a hash to an object as that object's members
// credit to: GoreScript
AS.inherit = function(classObj, members) {
	var base = Object.create(classObj.prototype);

	Object.getOwnPropertyNames(members).forEach(function(prop) {
		var desc = Object.getOwnPropertyDescriptor(members, prop);

		if (desc.get !== undefined) {
			base.__defineGetter__(prop, desc.get);
		}
		else {
			base[prop] = members[prop];
		}

		if (desc.set !== undefined) {
			base.__defineSetter__(prop, desc.set);
		}
	});

	return base;
};

AS.stringToFunction = function(str) {
    var arr = str.split(".");
    var fn = (window || this);

    for (var i = 0, len = arr.length; i < len; i++) {
        fn = fn[arr[i]];
    }

    if (typeof fn !== "function") {
        throw new Error("function not found");
    }
    return fn;
};

AS.logOnce = function(id, str) {
	if (AS.logOnce[id.toString()] === true) {
		return;
	}

	AS.logOnce[id.toString()] = true;
	console.log(str);
};

AS.isInt = function(n) {
    return Number(n) === n && n % 1 === 0;
};

AS.isFloat = function(n) {
    return n === Number(n) && n % 1 !== 0;
};

AS.Base = function() {
	this.clearColor = 0x000000;
	this.antialias = true;

    this.timeStep = 0.01666;

	this.cameraFov = 80;
	this.cameraNear = 0.1;
	this.cameraFar = 10000;

    this.agentViewCameraSize = [180, 101];

    this.additionalRenderers = {};
};

AS.Base.prototype = {
	constructor: AS.Base,

	init: function() {
		var self = this;

		$(document).on("contextmenu", function() {
			//return false;
		});

		this.renderer = new THREE.WebGLRenderer({ antialias: this.antialias });
		this.renderer.setClearColor(this.clearColor, 1);
		this.renderer.setSize(window.innerWidth, window.innerHeight);
		this.renderer.domElement.id = "simulator-canvas";

		this.camera = new THREE.PerspectiveCamera(
			this.cameraFov,
			window.innerWidth / window.innerHeight,
			this.cameraNear, this.cameraFar
		);

		this.scene = new THREE.Scene();


		window.addEventListener("resize", function() { self.onResize(); },
			false);
		this.onResize();

		document.body.appendChild(this.renderer.domElement);

		this.load();
	},

	onResize: function() {
		this.camera.aspect = window.innerWidth / window.innerHeight;
		this.camera.updateProjectionMatrix();

        if (ENV) {
            ENV.callOnAgents(function (agent) {
                if (agent instanceof AS.Agent3D) {
                    agent.updateCameraAspects();
                }
            });
        }

		this.renderer.setSize(window.innerWidth, window.innerHeight);
	},

	load: function() {
        if (this.requestAnimationFrameId !== null) {
            cancelAnimationFrame(this.requestAnimationFrameId);
        }

        this.currentTime = this.getCurrentTime();
        this.accumulator = 0.0;

        this.mainLoop();
	},

    // http://gafferongames.com/game-physics/fix-your-timestep/
    // http://timeinvariant.github.io/gorescript/
	mainLoop: function(frametime) {
        frametime = frametime || 0;
		var that = this;

		var newTime = this.getCurrentTime();
		var frameTime = (newTime - this.currentTime) / 1000;
		if (frameTime > 0.33) {
			frameTime = 0.33;
		}
		this.currentTime = newTime;
		this.accumulator += frameTime;

		while (this.accumulator >= this.timeStep) {
            TWEEN.update(frametime);
			this.update();
		 	this.accumulator -= this.timeStep;
		}

		this.draw();
        this.drawAdditional();
		this.requestAnimationFrameId = requestAnimationFrame(function(time) {
            that.mainLoop(time);
        });
	},

    reload: function() {
        this.scene = new THREE.Scene();
    },

    // function to be overloaded - update scene
    update: function() {
	},

    draw: function() {
		this.renderer.render(this.scene, this.camera);
	},

    drawAdditional: function() {},

    drawWithCamera: function(camera) {
        this.renderer.render(this.scene, camera);
    },

    getCurrentTime: function() {
        return Date.now();
    },
};
