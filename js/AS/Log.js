AS.Log = function() {

};

AS.Log.append = function(title, content) {
    var log = $("#log");
    var time = SIM.time / 1000;
    log.append('<p><span class="time">{0}</span> <title>{1}</title> {2}</p>'.format(
        time.toFixed(4),
        title,
        content
    ));
    log.animate({ scrollTop: log[0].scrollHeight }, "slow");
};

AS.Log.clear = function() {
    var log = $("#log");
    log.empty();
    log.append('<title>Agent log</title>');
};
