/**
 * Represents an agent's understanding of the surrounding terrain.
 */
AS.Terrain = function(knowledge) {
    this.K = knowledge;
    this.repr = [];

    this.init();
};

AS.Terrain.prototype = {
    /**
     * Initializes the terrain representation.
     */
    init: function() {
        for (var i = 0; i <= this.K.maxdepth; i++) {
            this.repr.push(new Array());

            for (var j = 0; j <= this.K.maxdepth; j++) {
                this.repr[i].push(new Array());

                for (var k = 0; k <= this.K.maxdepth; k++) {
                    // all we know for certain is that this isn't the origin -
                    // other properties are unknown right now
                    this.repr[i][j][k] = {};
                    this.setCellDefaults(i, j, k);
                }
            }
        }

        // set where the origin is
        this.repr
            [this.K.origin[0]]
            [this.K.origin[1]]
            [this.K.origin[2]].origin = true;
    },

    /**
     * Sets a cell to its default values.
     * @param {Integer} X coordinate
     * @param {Integer} Y coordinate
     * @param {Integer} Z coordinate
     */
    setCellDefaults: function(x, y, z) {
        this.repr[x][y][z] = {};

        this.repr[x][y][z].x = x;
        this.repr[x][y][z].y = y;
        this.repr[x][y][z].z = z;
        this.repr[x][y][z].origin = false;
    },

    setOccupied: function(x, y, z, value) {
        this.setFlag(x, y, z, 'occupied', value);
    },

    getOccupied: function(x, y, z) {
        var occupiedFlag = this.getFlag(x, y, z, 'occupied');
        var blockFlag = this.getFlag(x, y, z, 'block');
        var agentFlag = this.getFlag(x, y, z, 'agent');

        if (blockFlag || agentFlag || occupiedFlag)
            return true;

        if (blockFlag == undefined && agentFlag == undefined && occupiedFlag == undefined)
            return undefined;
        return false;
        /*
        if (blockFlag == undefined)
            return flag;
        if (agentFlag == undefined)
            return flag && blockFlag;
        return flag && agentFlag && blockFlag;*/
    },

    isIntended: function(x, y, z) {
        var flag = this.getFlag(x, y, z, 'intent');
        if (flag == undefined)
            return false;

        if (flag) return true;
        return false;
    },

    /**
     * Flags a certain cell with a flag & optional value.
     * @param {Integer} X coordinate
     * @param {Integer} Y coordinate
     * @param {Integer} Z coordinate
     * @param {String} Flag name (eg. 'block')
     * @param {*} Optional vlue to assign to the flag. Defaults to true if not
     *            specified.
     */
    setFlag: function(x, y, z, flag, value) {
        if (typeof value === 'undefined')
            value = true;

        this.prepare(x, y, z);
        this.repr[x][y][z][flag] = value;
    },

    /**
     * Gets the value of a flag for a certain cell.
     * @param {Integer} X coordinate
     * @param {Integer} Y coordinate
     * @param {Integer} Z coordinate
     * @param {String} Flag name (eg. 'block')
     * @return {*} Value of given flag.
     */
    getFlag: function(x, y, z, flag) {
        if (this.repr[x] && this.repr[x][y] && this.repr[x][y][z] &&
            this.repr[x][y][z][flag]) {
            return this.repr[x][y][z][flag];
        }
        return undefined;
    },

    /**
     * Updates state of terrain knowledge. Should be called regularly.
     */
    tick: function() {
        var self = this;

        // invalidate intents when their duration is over
        this.applyToAll(function(cell) {
            if (self.getFlag(cell.x, cell.y, cell.z, 'intent') &&
                self.repr[cell.x][cell.y][cell.z].intent.time >=
                    Date.now() + self.K.intentValidity) {

                self.setFlag(cell.x, cell.y, cell.z, 'intent', null);
            }
        });
    },

    /**
     * Applies a function to every cell in the terrain knowledge representation.
     * @param {Function} Function to apply.
     */
    applyToAll: function(f) {
        for (var i = 0; i < this.repr.length; i++)
            for (var j = 0; j < this.repr[i].length; j++)
                for (var k = 0; k < this.repr[i][j].length; k++)
                    f(this.repr[i][j][k]);
    },

    /**
     * Ensures the terrain representation is wide enough to cover point (x,y,z).
     * @param {Integer} X coordinate
     * @param {Integer} Y coordinate
     * @param {Integer} Z coordinate
     */
    prepare: function(x, y, z) {
        if (this.repr.length <= x) {
            this.expand('x', 1 + x - this.repr.length);
        }
        if (this.repr[x].length <= y) {
            this.expand('y', 1 + y - this.repr[x].length);
        }
        if (this.repr[x][y].length <= z) {
            this.expand('z', 1 + z - this.repr[x][y].length);
        }
    },

    /**
     * Expands the terrain representation array at a certain level (x, y or z)
     * by n places.
     * @param {String} 'x' for the first dimension, 'y' for second, 'z' for third.
     * @param {Integer} Amount to expand by.
     */
    expand: function(level, n) {
        switch (level) {
            case 'x':
                for (var i = 0; i < n; i++) {
                    this.repr.push(new Array());

                    for (var j = 0; j < this.repr[0].length; j++) {
                        this.repr[this.repr.length - 1].push(new Array());

                        for (var k = 0; k < this.repr[0][0].length; k++) {
                            this.repr[this.repr.length - 1]
                                [this.repr[this.repr.length - 1].length - 1]
                                .push({});

                            this.setCellDefaults(
                                this.repr.length - 1, /* last x in repr */
                                this.repr[this.repr.length - 1].length - 1,
                                this.repr
                                    [this.repr.length - 1]
                                    [this.repr[this.repr.length - 1].length - 1]
                                    .length - 1
                            );
                        }
                    }
                }
                break;
            case 'y':
                for (var i = 0; i < this.repr.length; i++) {
                    for (var j = 0; j < n; j++) {
                        this.repr[i].push(new Array());

                        for (var k = 0; k < this.repr[i][0].length; k++) {
                            this.repr[i][this.repr[i].length - 1].push({});

                            this.setCellDefaults(
                                i,
                                this.repr[this.repr.length - 1].length - 1,
                                this.repr
                                    [i]
                                    [this.repr[this.repr.length - 1].length - 1]
                                    .length - 1
                            );
                        }
                    }
                }
                break;
            case 'z':
                for (var i = 0; i < this.repr.length; i++) {
                    for (var j = 0; j < this.repr[i].length; j++) {
                        for (var k = 0; k < n; k++) {
                            this.repr[i][j].push({});

                            this.setCellDefaults(i, j, this.repr[i][j].length - 1);
                        }
                    }
                }
                break;
        }
    },

};
