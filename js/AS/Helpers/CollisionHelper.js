AS.CollisionHelper = {};

/**
 * Get a list of all possible obstacles along a certain line from a point.
 * @param {Vector3 object} Point from which to raycast.
 * @param {Vector3 object} Direction in which to raycast.
 * @param {List} List of objects that are collidable (solid).
 * @return {List} List of potential obstacles in that path.
 */
AS.CollisionHelper.getPotentialObstacles = function(point, direction, collidableMeshes) {
    var ray = new THREE.Raycaster(point, direction.clone().normalize());
    var collisions = ray.intersectObjects(collidableMeshes, true);

    var obstacles = [];

    $.each(collisions, function(ci, collision) {
        var add = { agent:false, collision: collision };

        // check if collision is part of an agent's mesh
        if (collision.object.userData && collision.object.userData.parentAgent) {
            add.agent = true;

            $.each(obstacles, function(oi, obstacle) {
                if (obstacle.agent &&
                    obstacle.collision.object.userData.parentAgent ==
                        collision.object.userData.parentAgent) {
                    // replace if this part of agent is nearer
                    if (collision.distance < obstacle.collision.distance) {
                        obstacles[oi] = add;
                    }
                    // don't add again
                    add = null;
                }
            });
        }

        if (add)
            obstacles.push(add);
    });

    return obstacles;
};

AS.CollisionHelper.isDangerousToMove = function() {
    
};
