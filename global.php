<?php

@ini_set('display_errors', 'On');
error_reporting(E_ALL);

// for handling DOM validation errors
libxml_use_internal_errors(true);

define('CWD', (getcwd() ? getcwd() : '.'));

?>
