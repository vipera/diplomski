
AS.DialogHelper = function() {
    this.dialogCount = 0;
};

AS.DialogHelper.showInputDialog = function(title, content, onSubmit, onClose) {
    AS.DialogHelper.show(title, content, onSubmit, onClose, true);
};

AS.DialogHelper.showInfoDialog = function(title, content, onClose, okbutton) {
    AS.DialogHelper.show(title, content, null, onClose, false);
};

AS.DialogHelper.addAgent = function(onAdd) {
    var dialog = $("#dialog-form").dialog({
          autoOpen: false,
          height: 300,
          width: 350,
          modal: true,
          buttons: {
            "Add": onAdd,
            Cancel: function() {
              dialog.dialog( "close" );
            }
          },
          close: function() {
            form[ 0 ].reset();
            allFields.removeClass( "ui-state-error" );
          }
        });

        var form = dialog.find("form").on("submit", function( event ) {
          event.preventDefault();
          onAdd();
        });
};

AS.DialogHelper.show = function(title, content, onSubmit, onClose, okbutton) {
    this.dialogCount++;
    var selector = 'div#dialog-{0}'.format(this.dialogCount);

    $('body').append(
        '<div class="dialog" id="dialog-{0}" title="Dialog" style="display:none;"></div>'
        .format(this.dialogCount));

    $(selector).attr('title', title);
    $(selector).html(content);

    if (okbutton != undefined && okbutton) {
        $(selector).dialog({
            width: 400,
            buttons : [{
                text: "Ok",
                click: function() {
                    //$(this).dialog("close"); //closing on Ok click
                    if (onSubmit)
                        onSubmit();
                    $(this).dialog("close");
                }
            }],
        });
    }
    else  {
        $(selector).dialog();
    }


    $(selector).on('dialogclose', function(event) {
        if (onClose)
            onClose();
        $(selector).dialog('destroy');
        $(selector).remove();
    });
}

/**
 * Shows the "scenario loader" from which a scenario can be selected
 * @param {Function} A callback with params (event, selected). The selected
 *  value will be returned to the callback.
 * @return {Null}
 */
AS.DialogHelper.showScenarioLoader = function(handler) {
    $.getJSON("manifest-json.php", function(data) {
        var options = [];
        $.each(data['scenarios'], function(i, scenario) {
            options.push(scenario.scenario_id);
        });
        var scenarioSelect = AS.HTMLHelper.selectFromList('scenario', options);

        var content = '<p>Please select a scenario to be loaded:</p>{0}' +
            '<p class="scenario-description">' + data['scenarios'][0]['description'] + '</p>';

        // set up event for showing description
        $(document).on('change', 'select#scenario', function(e) {
            $("select#scenario option:selected").each(function() {
                var selected_offset = $(this).val();
                $('div.dialog p.scenario-description')
                    .html(data['scenarios'][selected_offset]['description']);
            });
        });

        AS.DialogHelper.show('Load scenario',
            content.format(scenarioSelect),
            function(event) {
                var selected = $('div.dialog > select > option:selected').text();
                handler(event, selected);
            },
            null,
            true);
    });
}
