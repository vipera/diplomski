AS.HTMLHelper = {};

AS.HTMLHelper.ListType = {
    Ordered: 1,
    Unordered: 2,
};

/**
 * Constructs a HTML select tag with options
 * @param {String} Name and ID of select tag.
 * @param {Object} Key: value, Value: name.
 * @return {String} HTML select tag
 */
AS.HTMLHelper.selectFromList = function(name, items, options) {
    options = options || {};

    var html = '<select name="{0}" id="{0}">'.format(name, options.id || name);

    $.each(items, function(k, v) {
        html += '<option value="{0}"{1}>{2}</option>'.format(k, options.selected && k == options.selected ? ' selected="selected"' : '', v);
    });

    return html + '</select>';
};

/**
 * Constructs a HTML list (ordered / unordered) of elements.
 * @param {Integer} AS.HTMLHelper.ListType.Ordered or .Unordered (ol / ul)
 * @param {Array} Array of list item objects (must have contents property,
 *  optionally an options property)
 * @param {Object} Object representing attributes to add to ul/ol tag.
 * @return {String} List
 */
AS.HTMLHelper.makeList = function(listtype, items, listoptions) {
    listoptions = listoptions || {};

    var html = "<{0}".format(listtype == AS.HTMLHelper.ListType.Ordered ? "ol" : "ul");

    for (var key in listoptions)
        html += ' {0}="{1}"'.format(key, listoptions[key]);

    html += ">";

    $.each(items, function (index, item) {
        var lihtml = "<li";
        if (item.options) {
            for (var key in item.options)
                lihtml += ' {0}="{1}"'.format(key, item.options[key]);
        }
        lihtml += '>{0}</li>'.format(item.contents);

        html += lihtml;
    });

    html += "</{0}>".format(listtype == AS.HTMLHelper.ListType.Ordered ? "ol" : "ul");
    return html;
};
