/**
 * Menu
 */
AS.Menu = function(options) {
    this.icontheme = 'windows-phone-light';
    this.options = [
        [
            this.makeIconButton('startsimulation',
                'control.play',
                'Start simulation',
                function(button, event) {
                    button.toggle();

                    if (button.on)
                        SIM.unpause();
                    else
                        SIM.pause();
                },
                {
                    updateMenuAfterClick: true,
                    onOtherButtonClick: function(button, other) {
                        if (other.id == 'pausesimulation') {
                            if (other.on)
                                button.on = false;
                            else
                                button.on = true;
                        }
                        else if (other.id == 'restartsimulation')
                            button.on = false;
                    },
                }
            ),
            this.makeIconButton('pausesimulation',
                'control.pause',
                'Pause simulation',
                function(button, event) {
                    button.on = true;
                    SIM.pause();
                },
                {
                    defaultState: AS.Menu.Button.States.On,
                    updateMenuAfterClick: true,
                    onOtherButtonClick: function(button, other) {
                        if (other.id == 'startsimulation') {
                            if (other.on)
                                button.on = false;
                            else
                                button.on = true;
                        }
                        else if (other.id == 'restartsimulation')
                            button.on = true;
                    },
                }
            ),
            this.makeIconButton('restartsimulation',
                'refresh',
                'Restart simulation',
                function(button, event) {
                    SIM.restart();
                },
                { updateMenuAfterClick: true, }
            ),
        ],
        [
            this.makeTextButton('agentoptionstext', 'Agent options', null),
            this.makeIconButton('agentview',
                'camera.switch',
                'Show agent view',
                function(button, event) {
                    button.menu.agentView = !button.menu.agentView;
                    button.toggle();

                    // show agent view
                    if (button.menu.agentView) {
                        var AID = $('#menuitem-agentselect option:selected').val();
                        AID = AID.substring(1, AID.length);
                        button.menu.viewingAgent = AID;

                        $('#agentview').show();
                    }
                    // hide agent view
                    else {
                        $('#agentview').hide();
                        $('#agentview').empty();

                        button.menu.viewingAgent = 0;
                        SIM.disableAgentView();
                    }
                },
                {
                    updateMenuAfterClick: true,
                }
            ),
            this.makeSelecter('agentselect',
                'Agent select',
                {'N0': 'N/A'},
                function(selecter, event) {
                    var AID = selecter.selectedValue;
                    if (!AID)
                        return;

                    AID = AID.substring(1, AID.length);
                    selecter.menu.viewingAgent = AID;

                    SIM.disableAgentView();
                    $('#agentview').empty();

                    var agent = ENV.getAgent(AID);

                    if (agent) {
                        var behaviourType = agent.behaviour.name;
                        $('#menuitem-agentbehaviour option:contains("{0}")'.format(behaviourType))
                            .prop('selected', true);
                    }
                },
                {}
            ),
            this.makeIconButton('agentnew', 'add', 'Add new agent to scene',
                function(button, event) {
                    var dialog, form;
                    var onAdd = function() {
                        var type = $("#agent-type").val();
                        var behaviour = $("#agent-behaviour option:selected").text();
                        var position = $("#agent-position").val();
                        var direction = $("#agent-direction").val();

                        var pos = position.split(',').map(function(item) { return 1*item.trim(); });
                        var dir = direction.split(',').map(function(item) { return 1*item.trim(); });

                        SIM.addAgent(type, 'AS.Behaviours.{0}'.format(behaviour),
                            new THREE.Vector3(pos[0], pos[1], pos[2]),
                            new THREE.Vector2(dir[0], dir[1]));

                        dialog.dialog( "close" );
                        return true;
                    };

                    dialog = $("#dialog-form").dialog({
                          autoOpen: false,
                          height: 360,
                          width: 380,
                          modal: true,
                          buttons: {
                            "Add": onAdd,
                            Cancel: function() {
                              dialog.dialog( "close" );
                            }
                          },
                          close: function() {
                            form[ 0 ].reset();
                          }
                        });

                        form = dialog.find("form").on("submit", function( event ) {
                          event.preventDefault();
                          onAdd();
                        });
                    dialog.dialog( "open" );
                }
            ),
            this.makeIconButton('agentremove', 'delete', 'Remove selected agent from scene',
                function(button, event) {
                    var AID = $('#menuitem-agentselect option:selected').val();
                    AID = AID.substring(1, AID.length);

                    SIM.removeAgent(AID);
                    button.menu.makeAgentList();
                },
                {
                    updateMenuAfterClick: true,
                }
            ),
            this.makeSelecter('agentbehaviour',
                'Agent behaviour',
                options.behaviours,
                function(selecter, event) {
                },
                {}
            ),
            this.makeIconButton('agentsave',
                'people.arrow.right',
                'Apply behaviour to agent',
                function(button, event) {
                    //var items_in_group = button.getAllItemsInGroup();
                    //console.log(items_in_group);
                    var AID = $('#menuitem-agentselect option:selected').val();
                    AID = AID.substring(1, AID.length);

                    var behaviour = $('#menuitem-agentbehaviour option:selected').text();
                    var behaviourObj = AS.stringToFunction('AS.Behaviours.{0}'.format(behaviour));

                    SIM.changeAgentBehaviour(AID, new behaviourObj());
                },
                {}
            ),
        ],
        [
            this.makeIconButton('axishelper',
                'axis.three',
                'Show axis helper',
                function(button, event) {
                    button.toggle();

                    if (button.on) {
                        SIM.showAxisHelper();
                    }
                    else {
                        SIM.hideAxisHelper();
                    }
                },
                {
                    updateMenuAfterClick: true,
                    defaultState: AS.Menu.Button.States.On,
                }
            ),
            this.makeIconButton('animation-speed',
                'timer.forward',
                'Adjust animation speed',
                function(button, event) {
                    AS.DialogHelper.showInputDialog('Adjust animation speed',
                        'Animation speed: <div id="menuitem-animation-speed-slider"></div>' +
                        'Currently at <span id="menuitem-animation-speed-display">{0}</span> ms/animation.'
                        .format(SIM.animationDuration),
                        function() {
                            var animationSpeed = $("#menuitem-animation-speed-slider").slider("option", "value");
                            SIM.animationDuration = animationSpeed;
                        },
                        null
                    );
                    $("#menuitem-animation-speed-slider").slider({
                        min:100,
                        max: 5000,
                        step:50,
                        value: SIM.animationDuration,
                        slide: function( event, ui ) {
                            $('#menuitem-animation-speed-display').text(
                                $("#menuitem-animation-speed-slider").slider('value')
                            );
                        },
                    });
                },
                {
                }
            ),
            this.makeIconButton('show-log',
                'page.text',
                'Show log',
                function(button, event) {
                    button.toggle();

                    if (button.on) {
                        $('#log').show();
                    }
                    else {
                        $('#log').hide();
                    }
                },
                {
                    updateMenuAfterClick: true,
                }
            ),
            /*this.makeIconButton('scene1',
                'image',
                '?',
                function(button, event) {
                    ENV.unregisterAgent(1);
                    button.menu.makeAgentList();
                },
                {
                    updateMenuAfterClick: true,
                }
            ),*/
        ],
    ];

    this.agentList = {};

    this.agentView = false;
    this.viewingAgent = 0;
    $('#agentview').hide();

    $.each(options.behaviours, function(bi, behaviour) {
        $('#agent-behaviour').append('<option value="{0}">{1}</option>'.format(bi, behaviour));
    });

};

AS.Menu.prototype = {
    constructor: AS.Menu,

    get currentAgentForAgentView() {

    },

    makeAgentList: function() {
        var agents = ENV.getAllAgents();
        var list = {};
        $.each(agents, function(ai, agent) {
            list['A' + agent.AID] = agent.name;
        });
        this.agentList = list;
        return list;
    },

    updateMenu: function() {
        var groups = [];

        this.makeAgentList();
        // XXX: Ugly hack!! Halp!
        this.options[1][2].items = this.agentList;

        $.each(this.options, function(index, group) {
            var children = [];
            $.each(group, function(cindex, item) {
                children.push({ contents: item.render() });
            });

            groups.push({ contents: AS.HTMLHelper.makeList(AS.HTMLHelper.ListType.Ordered,
                children, { class:'buttons' }) });
        });

        var menuhtml = AS.HTMLHelper.makeList(AS.HTMLHelper.ListType.Ordered,
            groups, { class:'groups' });
        $('#toolbar').html(menuhtml);

        // XXX: Ugly hack part 2!
        $('#menuitem-agentselect').trigger("change");
    },

    buttonClicked: function(button) {
        // notify other buttons that are concerned that a button has been clicked
        $.each(this.options, function(index, group) {
            $.each(group, function(iindex, item) {
                if (item.id != button.id && item.options.onOtherButtonClick)
                    item.options.onOtherButtonClick(item, button);
            });
        });
    },

    getAllItemsInGroup: function(item) {
        var sought_after = [];
        $.each(this.options, function(group_index, group) {
            $.each(group, function(item_index, gitem) {
                if (gitem.id == item.id) {
                    sought_after = group;
                }
            });
        });
        return sought_after;
    },

    makeIconButton: function(id, icon, description, callback, options) {
        return new AS.Menu.Button(this, id, AS.Menu.Button.Types.Icon, icon,
            description, callback, options);
    },

    makeTextButton: function(id, text, callback, options) {
        return new AS.Menu.Button(this, id, AS.Menu.Button.Types.Text, text,
            text, callback, options);
    },

    makeSelecter: function(id, title, items, callback, options) {
        return new AS.Menu.Selecter(this, id, title, items, callback, options);
    },

    getIconUrl: function(iconname) {
        return 'images/icons/{0}/{1}.png'.format(this.icontheme, iconname);
    },
};

/**
 * Button
 */
AS.Menu.Button = function(menu, id, type, content, description, callback, options) {
    this.options = options || {};
    // all buttons are off by default
    this.state = this.options.defaultState || AS.Menu.Button.States.Off;

    this.menu = menu;
    this.id = id;
    this.htmlid = 'menuitem-{0}'.format(id);

    this.type = type;
    this.content = content;
    this.description = description;
    this.callback = callback;
};
AS.Menu.Button.Types = { Icon: 1, Text: 2 };
AS.Menu.Button.States = { Off: 0, On: 1 };

AS.Menu.Button.prototype = {
    constructor: AS.Menu.Button,

    get on() {
        return this.state == AS.Menu.Button.States.On;
    },

    set on(value) {
        this.state = value ? AS.Menu.Button.States.On : AS.Menu.Button.States.Off;
    },

    toggle: function() {
        if (this.on)
            this.on = false;
        else
            this.on = true;
    },

    getAllItemsInGroup: function() {
        return this.menu.getAllItemsInGroup(this);
    },

    render: function() {
        var html = '';

        if (this.type == AS.Menu.Button.Types.Icon) {
            html += '<a';

            if (this.state == AS.Menu.Button.States.On)
                html += ' class="on"';
            html += ' href="#">'
        }

        switch (this.type) {
            case AS.Menu.Button.Types.Icon:
                html += '<img id="{0}" src="{1}" alt="{2}" title="{2}">'.format(
                    this.htmlid,
                    this.menu.getIconUrl(this.content),
                    this.description
                );
                html += '</a>';
                break;

            case AS.Menu.Button.Types.Text:
                html += '<p id="{0}">{1}</p>'.format(this.htmlid, this.content);
                break;
        }

        $(document).off('click', '#' + this.htmlid); // remove all click event handlers

        var self = this;
        $(document).on('click', '#' + this.htmlid, function(e) {
            if (self.callback)
                self.callback(self, e);

            // alert menu that button has been clicked
            self.menu.buttonClicked(self);

            // update menu after click if necessary
            if (self.options.updateMenuAfterClick) {
                self.menu.updateMenu();
            }
        });

        return html;
    },
};

/**
 * Selecter
 */
AS.Menu.Selecter = function(menu, id, title, items, callback, options) {
    this.options = options || {};
    this.menu = menu;

    this.id = id;
    this.htmlid = 'menuitem-{0}'.format(id);

    this.title = title;
    this.items = items;
    this.callback = callback;

    this.selectedValue = Object.keys(items)[0];
    this.selectedIndex = options.defaultSelectedIndex || 0;
};

AS.Menu.Selecter.prototype = {
    constructor: AS.Menu.Selecter,

    render: function() {
        var self = this;
        var html = '<title>{0}</title>'.format(this.title);

        html += AS.HTMLHelper.selectFromList(this.htmlid, this.items, {
            id: this.htmlid,
            selected: Object.keys(this.items)[this.selectedIndex]
        });

        $(document).off('change', '#' + this.htmlid);
        $(document).on('change', '#' + this.htmlid, function(e) {
            self.selectedValue = $(this).val();
            self.selectedIndex = $(this)[0].selectedIndex;
            self.callback(self, e);
        });

        return html;
    },
};
