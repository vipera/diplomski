AS.Agent3D = function(behaviour) {
    // coordinates relative to the scene
    this.coordinates = new THREE.Vector3(0, 0, 0);

    // THREE.js world position
    this.position = new THREE.Vector3(0, 0, 0);
    this.direction = new THREE.Vector3(0, 0, 1);

    this._object = null;
    this._boundingBoxObject = null;
    this._meshes = {};

    // cameras this agent has
    this.cameras = [
        {
            id: 'camera-ahead',
            name: 'Ahead',
            camera: new THREE.PerspectiveCamera(
                SIM.cameraFov,
    			160/90,
    			SIM.cameraNear, SIM.cameraFar
            ),
            change: function(agent, camera) {
                camera.position.copy(agent.position);
                camera.lookAt(
                    camera.position.clone().add(agent.direction)
                );
            },
        },
    ];

    // animations to be performed
    this.animations = [];

    // unit distance (width of one block) for animations
    this.unitdistance = ENV.getUnitDistance();

    this.updateCameras();

    AS.Agent.call(this, behaviour);

    // origin block for synchronization - if provided
    this.K.origin = ENV.getOrigin();
};

AS.Agent3D.Actions = AS.inherit(AS.Agent.Actions, {
    /**
     * Possible directions an agent can go in 3D space.
     */
    Movement: {
        Up:                2001,
        UpForward:         2002,
        UpForwardLeft:     2003,
        UpForwardRight:    2004,
        UpLeft:            2005,
        UpRight:           2006,
        UpBack:            2007,
        UpBackLeft:        2008,
        UpBackRight:       2009,

        Forward:          2010,
        ForwardLeft:      2011,
        ForwardRight:     2012,
        Left:             2013,
        Right:            2014,
        Backward:         2015,
        BackwardLeft:     2016,
        BackwardRight:    2017,

        Down:             2018,
        DownForward:      2019,
        DownForwardLeft:  2020,
        DownForwardRight: 2021,
        DownLeft:         2022,
        DownRight:        2023,
        DownBack:         2024,
        DownBackLeft:     2025,
        DownBackRight:    2026,
    },
    Rotation: {
        Left:  2030,
        Right: 2031,
        About: 2032, // 180-deg turn
    }
});

AS.Agent3D.prototype = AS.inherit(AS.Agent, {
    constructor: AS.Agent3D,

    /**
     * Gets THREE.js object that represents this agent graphically.
     */
    get object() {
        if (!this._object) {
            this._object = new THREE.Object3D();
            for (var key in this._meshes) {
                this._object.add(this._meshes[key]);
            }

            var self = this;
            $.each(this._object.children, function(ci, child) {
                child.userData.parentAgent = self.AID;
            });

            this._object.lookAt(this.direction);
            this._object.position.copy(this.position);
            this._object.up.copy(AS.SpatialHelper.UpVector);
        }
        return this._object;
    },

    initKnowledge: function() {
        AS.Agent.prototype.initKnowledge.call(this);

        // mark current position as visited
        this.K.terrain.setFlag(this.K.position.x, this.K.position.y,
            this.K.position.z, 'visited', true);
    },

    perceiveSurroundings: function() {
        if (!this.nextActionState.performing) {
            // look around with collisionhelper
            var self = this;
            var point = this.position;
            var directions = AS.SpatialHelper.allDirectionVectors;

            $.each(directions, function(di, dir) {
                var pos = AS.SpatialHelper.nextRelativePositionInDirection(
                    self.K.position,
                    dir
                );
                if (!AS.SpatialHelper.validRelativePosition(pos))
                    return;

                self.K.terrain.setOccupied(pos.x, pos.y, pos.z, false);

                var obstacles = AS.CollisionHelper.getPotentialObstacles(point, dir,
                    ENV.getCollidableObjects(self));

                $.each(obstacles, function(oi, obstacle) {
                    if (obstacle.collision.distance <= self.K.collisionThreshold) {
                        //self.K.terrain.setOccupied(pos.x, pos.y, pos.z, false);
                        if (obstacle.agent) {
                            self.K.terrain.setOccupied(pos.x, pos.y, pos.z, true);
                        }
                        else {
                            // this is terrain (block)
                            self.K.terrain.setFlag(pos.x, pos.y, pos.z, 'block');
                        }
                    }
                });
            });
        }

        AS.Agent.prototype.perceiveSurroundings.call(this);
    },

    /**
     * Updates camera aspect ratios. Will be called by Environment when screen
     * changes.
     */
    updateCameraAspects: function() {
        /*$.each(this.cameras, function (index, camera) {
            if (camera.camera) {
                camera.camera.aspect = window.innerWidth / window.innerHeight;
        		camera.camera.updateProjectionMatrix();
            }
        });*/
    },

    updateCameras: function() {
        var self = this;
        $.each(this.cameras, function(ci, camera) {
            camera.change(self, camera.camera);
        });
    },

    performAction: function() {
        var self = this;

        // propagate call to agent
        if (AS.Agent.prototype.performAction.call(this))
            return true;


        if (this.knowsAction(2000, this.nextAction)) {
            // this is an Agent3D action
            var animation = null;
            var movement = false;

            var rotationTween = null;

            switch (this.nextAction) {
                case AS.Agent3D.Actions.Rotation.Left:debugger;
                    animation = new AS.Animation.Tween(
                        this._object.rotation,
                        { y: self._object.rotation.y + Math.PI / 2 },
                        SIM.animationDuration
                    );
                    /*
                    var endDirection = this.direction.clone().applyAxisAngle(
                        AS.SpatialHelper.UpVector, //new THREE.Vector3(0, 1, 0),
                        Math.PI / 2
                    );*/
                    var endDirection = AS.SpatialHelper.relativeDirectionToWorldDirection(
                        AS.SpatialHelper.getNextRelativeDirection(this, this.nextAction)
                    );
                    rotationTween = new AS.Animation.Tween(
                        this.direction,
                        { x: endDirection.x, y: endDirection.y, z: endDirection.z },
                        SIM.animationDuration
                    );

                    this.K.nextDirection = AS.SpatialHelper.getNextRelativeDirection(this, this.nextAction);
                    break;
                case AS.Agent3D.Actions.Rotation.Right:
                    animation = new AS.Animation.Tween(
                        this._object.rotation,
                        { y: self._object.rotation.y - Math.PI / 2 },
                        SIM.animationDuration
                    );
                    /*var endDirection = this.direction.clone().applyAxisAngle(
                        AS.SpatialHelper.UpVector, //new THREE.Vector3(0, 1, 0),
                        -Math.PI / 2
                    );*/
                    var endDirection = AS.SpatialHelper.relativeDirectionToWorldDirection(
                        AS.SpatialHelper.getNextRelativeDirection(this, this.nextAction)
                    );
                    rotationTween = new AS.Animation.Tween(
                        this.direction,
                        { x: endDirection.x, y: endDirection.y, z: endDirection.z },
                        SIM.animationDuration
                    );
                    this.K.nextDirection = AS.SpatialHelper.getNextRelativeDirection(this, this.nextAction);
                    break;
                case AS.Agent3D.Actions.Rotation.About:
                    // decompose into two actions for ease of calculations...
                    this.K.aboutFace = true;
                    this.nextAction = AS.Agent3D.Actions.Rotation.Left;
                    this.nextActionState.performed = false;
                    this.nextActionState.performing = false;
                    /*
                    animation = new AS.Animation.Tween(
                        this._object.rotation,
                        { y: self._object.rotation.y + Math.PI },
                        SIM.animationDuration
                    );

                    var endDirection = //this.direction.clone().multiplyScalar(-1);
                        this.direction.clone().applyAxisAngle(
                            AS.SpatialHelper.UpVector,
                            Math.PI
                        );

                    rotationTween = new AS.Animation.Tween(
                        this.direction,
                        { x: endDirection.x, y: endDirection.y, z: endDirection.z },
                        SIM.animationDuration
                    );*/

                    //this.K.nextDirection = AS.SpatialHelper.getNextRelativeDirection(this, this.nextAction);
                    break;

                default: // it's a movement!

                    movement = true;
                    var movementTween = AS.SpatialHelper.getTweenObjectForMovement(
                        this._object.position,
                        this.direction,
                        this.nextAction,
                        this.unitdistance
                    );
                    this.K.nextPosition = AS.SpatialHelper.getNextRelativePosition(this, this.nextAction);
                    AS.Log.append('Agent A' + this.AID, 'moving to ({0},{1},{2})'.format(this.K.nextPosition.x, this.K.nextPosition.y, this.K.nextPosition.z));
                    animation = new AS.Animation.Tween(
                        this._object.position,
                        movementTween.to,
                        SIM.animationDuration
                    );
                    animation.interpolationType(movementTween.interpolation);
                    break;
            }

            if (animation) {
                self.nextActionState.performing = true;
                animation.onUpdate(function() {
                    self.position.copy(self._object.position);
                    self.updateCameras();
                });
                animation.onComplete(function() {
                    self.nextActionState.performed = true;
                    self.nextActionState.performing = false;

                    if (self.K.aboutFace) {
                        self.K.aboutFace = false;
                        self.nextAction = AS.Agent3D.Actions.Rotation.Left;
                        self.nextActionState.performed = false;
                    }

                    var fromPos = self.K.position;

                    if (self.K.nextPosition)
                        self.K.position = self.K.nextPosition;
                    if (self.K.nextDirection)
                        self.K.direction = self.K.nextDirection;

                    // mark this location as visited
                    self.K.terrain.setFlag(self.K.position.x, self.K.position.y,
                        self.K.position.z, 'visited', true);

                    // send message that agent has moved
                    self.sendBroadcast('MOVED', '{0},{1},{2} {3},{4},{5}'.format(
                        fromPos.x, fromPos.y, fromPos.z,
                        self.K.position.x, self.K.position.y, self.K.position.z
                    ));
                });
                animation.start();
                if (!movement && rotationTween) {
                    rotationTween.onUpdate(function() {
                        self.updateCameras();
                    });
                    rotationTween.start();
                }
                this.animations.push(animation);
            }
        }
    },

    updateAnimation: function() {

        // propagate call to agent
        AS.Agent.prototype.updateAnimation.call(this);
    },
});
