AS.Behaviours.SitStill = function() {
    this.className = "SitStill";
    this.description = "Agent will always perform a 'NoAction' action.";
};

AS.Behaviours.SitStill.prototype = AS.inherit(AS.Behaviour, {
    constructor: AS.Behaviours.SitStill,

    decideAction: function() {
        // ask underlying class for action
        var baseAction = AS.Behaviour.prototype.decideAction.call(this);
        if (baseAction)
            return baseAction;

        return AS.Agent.Actions.NoAction;
    },
});
