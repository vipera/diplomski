AS.GraphicsHelper = function() {
    this.skybox = null;
    this.modelBox = null;
    this.axisHelper = null;

    this.sidesCullingOffsets = {
        'up': 0,
        'down': 1,
        'left': 2,
        'right': 3,
        'front': 4,
        'back': 5,
    };
};

// http://www.jbernier.com/merging-geometries-in-three-js
// (THREE.GeometryUtils.merge is deprecated)
AS.GraphicsHelper.prototype.mergeGeometries = function(meshes) {
    var combined = new THREE.Geometry();

    for (var i = 0; i < meshes.length; i++) {
        meshes[i].updateMatrix();
        combined.merge(meshes[i].geometry, meshes[i].matrix);
    }
    return combined;
};

AS.GraphicsHelper.prototype.makeSceneGeometry = function(scenario) {
    var self = this;
    var geometries = [];

    if (this.modelBox == null) {
        this.modelBox = new THREE.BoxGeometry(
            scenario['unitlength'],
            scenario['unitlength'],
            scenario['unitlength'],
            1, 1, 1
        );
    }
    var geometry = new THREE.Geometry();

    $.each(scenario.scene, function(index, box) {
        if (!box['solid'])
            return;

        var matrix = new THREE.Matrix4();
        matrix.makeTranslation(
            box['x'],
            box['y'],
            box['z']
        );

        var boxGeometry = self.modelBox.clone();
        var cullingOffset = 0;
        $.each(box['culling'], function(cindex, cull) {
            if (cull) {
                boxGeometry.faces.splice(cullingOffset, 2);
            }
            else {
                cullingOffset += 2;
            }
        });

        geometry.merge(boxGeometry, matrix);
        //return false;
    });

    /*matrix.makeTranslation(
        x * 100 - worldHalfWidth * 100,
        h * 100,
        z * 100 - worldHalfDepth * 100
    );*/
    geometry.rotateX(-Math.PI / 2);
    geometry.rotateY(-Math.PI / 2);
    return geometry;//new THREE.Mesh(this.modelBox, matrix);
};

AS.GraphicsHelper.prototype.addAxisHelper = function(scene) {
    if (!this.axisHelper)
        this.axisHelper = new THREE.AxisHelper(100);
    scene.add(this.axisHelper);
};

AS.GraphicsHelper.prototype.removeAxisHelper = function(scene) {
    if (this.axisHelper)
        scene.remove(this.axisHelper);
};

AS.GraphicsHelper.prototype.makeSkybox = function(scene, size, skyboxtheme) {
    var imagePrefix = "images/skybox/{0}_".format(skyboxtheme);
    var directions  = ['left', 'right', 'top', 'bottom', 'back', 'front'];
    //["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"];
    var imageSuffix = ".jpg";

    var materialArray = [];
    for (var i = 0; i < 6; i++) {
        materialArray.push(new THREE.MeshBasicMaterial({
            map: THREE.ImageUtils.loadTexture(imagePrefix + directions[i] + imageSuffix),
            side: THREE.BackSide
        }));
    }
    var skyMaterial = new THREE.MeshFaceMaterial(materialArray);

    var skyGeometry = new THREE.CubeGeometry(size, size, size);
    this.skybox = new THREE.Mesh(skyGeometry, skyMaterial);

    scene.add(this.skybox);
};
