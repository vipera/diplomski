AS.Environment = function() {
    this.agents = [];
};

AS.Environment.prototype = {
    constructor: AS.Environment,

    /**
     * Register a created agent to the environment.
     * @param {Object} Agent object
     */
    registerAgent: function(agent) {
        var aid = this.agents.length + 1;
        agent.AID = aid;
        this.agents.push(agent);

        SIM.menu.makeAgentList();
        SIM.menu.updateMenu();
    },

    unregisterAgent: function(AID) {
        var agent = this.getAgent(AID);
        var self = this;

        // remove from list of agents
        $.each(this.agents, function(ai, a) {
            if (a.AID == AID) {
                self.agents.splice(ai, 1);
                return false;
            }
        });
    },

    unregisterAllAgents: function() {
        this.agents = [];
    },

    getAllAgents: function() {
        return this.agents;
    },

    /**
     * Write all registered agents to the console
     */
    debugListAgents: function() {
        $.each(this.agents, function(index, agent) {
            console.log(index + ": " + agent.name);
        });
    },

    /**
     * Call a function on all agents sequentially.
     * Example: ENV.callOnAgents(function (agent) { console.log(agent.name) });
     * @param {Function} function to apply to all agents
     */
    callOnAgents: function(f) {
        $.each(this.agents, function(index, agent) {
            f(agent);
        });
    },

    callOnAgent: function(AID, f) {
        var agent = this.getAgent(AID);
        f(agent);
    },

    getAgentCameras: function(AID) {
        var agent = this.getAgent(AID);

        if (agent.cameras)
            return agent.cameras;
        else
            return [];
    },

    getAgent: function(AID) {
        var foundAgent = null;
        $.each(this.agents, function(index, agent) {
            if (agent.AID == AID)
                foundAgent = agent;
        });
        return foundAgent;
    },

    /**
     * Call the "tick" method on all agents.
     */
    tickAgents: function() {
        $.each(this.agents, function(index, agent) {
            if (agent) // needed when agents complete their goals, unsure why
                agent.tick();
        });
    },

    getCollidableObjects: function(agent) {
        var collidable = [];

        // first add scene objects
        collidable.push(SIM.sceneMesh);

        // next add agents
        $.each(this.agents, function(index, a) {
            if (a.AID != agent.AID && a.object) {
                collidable.push(a.object);
                /*
                // add agent's bounding box
                var boundingBox = new THREE.Box3().setFromObject(a.object);
                var boundingSize = boundingBox.size();
                var boxGeom = new THREE.BoxGeometry(boundingSize.x, boundingSize.y, boundingSize.z);

                var mesh = new THREE.Mesh(boxGeom);
                mesh.position
                    .copy(boundingBox.max)
                    .add(boundingBox.min)
                    .divideScalar(2);

                SIM.scene.add(mesh);
                collidable.push(mesh);
                //collidable.push(a.object);
                */
            }
        });

        return collidable;
    },

    message: function(sender, to, ontology, message) {
        $.each(this.agents, function(index, agent) {
            if (to == agent.AID) {
                agent.receiveMessage(sender.AID, ontology, message);
                return true;
            }
        });
    },

    broadcast: function(sender, ontology, message) {
        $.each(this.agents, function(index, agent) {
            agent.receiveMessage(sender.AID, ontology, message);
        });
    },

    getUnitDistance: function() {
        if (SIM.scenarioData)
            return SIM.scenarioData.unitlength;
        return 0;
    },

    getOrigin: function() {
        if (SIM.scenarioData && SIM.scenarioData.origin)
            return SIM.scenarioData.origin;
        return null;
    },

    getCenter: function() {
        if (SIM.scenarioData && SIM.scenarioData.center)
            return SIM.scenarioData.center;
        return null;
    },

    informAboutGoalFulfilment: function(AID) {
        var agent = this.getAgent(AID);
        SIM.agentFulfilledGoal(agent);
    },
};
