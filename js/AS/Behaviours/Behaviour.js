// object in which all specific behaviours will be kept
AS.Behaviours = {};

// generic behaviour object
AS.Behaviour = function() {
    this.agent = null;
    this.className = "Behaviour";
    this.description = "Base behaviour, must be overriden by a concrete behaviour.";
};

AS.Behaviour.prototype = {
    constructor: AS.Behaviour,

    get name() {
        return this.className;
    },

    setAgent: function(agent) {
        this.agent = agent;
    },

    /**
     * Takes messages from the received message queue and interprets their
     * contents.
     */
    processMessages: function() {
        // INTENT_MOVE, MOVED, USELESS
        var self = this;
        this.agent.receivedMessages = $.grep(this.agent.receivedMessages, function(message, mi) {
            switch (message.ontology) {
                case 'INTENT_MOVE':
                    var msgParts = message.content.split(' ');
                    var fromArr = msgParts[0].split(',');
                    var toArr = msgParts[1].split(',');
                    self.agent.K.terrain.setFlag(toArr[0], toArr[1], toArr[2], 'intent', {
                        time: Date.now(),
                        agentid: message.from,
                        from: new THREE.Vector3(fromArr[0], fromArr[1], fromArr[2]),
                    });
                    self.agent.K.terrain.setFlag(fromArr[0], fromArr[1], fromArr[2], 'agent', message.from);
                    return false; // delete this message from queue
                case 'MOVED':
                    var msgParts = message.content.split(' ');
                    var fromArr = msgParts[0].split(',');
                    var toArr = msgParts[1].split(',');
                    // remove intent
                    self.agent.K.terrain.setFlag(fromArr[0], fromArr[1], fromArr[2], 'agent', null);
                    self.agent.K.terrain.setFlag(toArr[0], toArr[1], toArr[2], 'intent', null);
                    //self.agent.K.terrain.setFlag(toArr[0], toArr[1], toArr[2], 'agent', message.from);
                    return false; // delete this message from queue
                case 'USELESS':
                    var msgParts = message.content.split(' ');
                    var posArr = msgParts[0].split(',');
                    var direction = msgParts[1].split(',');

                    self.agent.K.terrain.setFlag(posArr[0], posArr[1], posArr[2], 'useless', {
                        from: new THREE.Vector3(direction[0], direction[1], direction[3]),
                    });
                    return false; // delete this message from queue
                case 'GOAL_FOUND':
                    var posArr = message.content.split(',');
                    self.agent.K.knownGoal = new THREE.Vector3(posArr[0],
                        posArr[1], posArr[2]);
                    return false;
            }


        });
    },

    goalFulfilled: function() {
        
    },

    /**
     * Decides an action. Return true when action is decided.
     * @return {Boolean} True if action was decided by this method.
     */
    decideAction: function() {
        if (!this.agent)
            return AS.Agent.Actions.NoAction;

        return null;
    },
};
