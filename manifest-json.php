<?php
require './global.php';

$dir = CWD . '/scenarios';

function cmp_scenarios($a, $b) {
    return strcmp($a['scenario_id'], $b['scenario_id']);
}

if (is_dir($dir))
{
    if ($dh = opendir($dir))
    {
        $files = array();

        while (($file = readdir($dh)) !== false)
        {
            if (!in_array($file, array('.', '..')) &&
                substr($file, -4) == '.xml') {

                $filename = $dir . '/' . $file;

                $data = array();
                if (is_readable($filename)) {
                    $testdoc = new DOMDocument();
                    $testdoc->load($filename);


                    if (!$testdoc->schemaValidate(CWD . '/scenarios/agent-setup.xsd'))
                    {
                        $data['description'] = '<span class="error">Scenario invalid.</span>';
                    }
                    else
                    {
                        $agent_setup = simplexml_load_file($filename);
                        $data['description'] = '<strong>Scenario description:</strong><br />' .
                            strval($agent_setup['description']);
                    }
                }

                $data['scenario_id'] = substr($file, 0, -4);
                $files[] = $data;
            }
        }
        closedir($dh);
        usort($files, 'cmp_scenarios');

        print(json_encode(array(
            'scenarios' => $files
        )));
    }
}

?>
