AS.Behaviours.SimpleDown = function() {
    this.className = "SimpleDown";
    this.description = "Agent will move straight down as far as possible.";
};

AS.Behaviours.SimpleDown.prototype = AS.inherit(AS.Behaviour, {
    constructor: AS.Behaviours.SimpleDown,

    decideAction: function() {
        var self = this;

        // ask underlying class for action
        var baseAction = AS.Behaviour.prototype.decideAction.call(this);
        if (baseAction)
            return baseAction;

        /*var collidable = ENV.getCollidableObjects(this.agent);
        var obstacles = AS.CollisionHelper.getPotentialObstacles(
            this.agent.position,
            AS.SpatialHelper.DownVector,
            collidable
        );

        var willCollide = false;
        $.each(obstacles, function(oi, obstacle) {
            if (obstacle.collision.distance <= self.agent.K.collisionThreshold)
                willCollide = true;
        });

        if (willCollide)
            return AS.Agent.Actions.NoAction;
        return AS.Agent3D.Actions.Movement.Down;
        */

        var nextZ = this.agent.K.position.z + 1;
        var occupied = this.agent.K.terrain.getOccupied(
            this.agent.K.position.x, this.agent.K.position.y, nextZ);

        if (occupied == undefined || occupied === false)
            return AS.Agent3D.Actions.Movement.Down;
        return AS.Agent.Actions.NoAction;
    },
});
