AS.Behaviours.Shark = function() {
    this.className = "Shark";
    this.description = "Move forwards until an obstacle is reached, turn around and repeat.";
};

AS.Behaviours.Shark.prototype = AS.inherit(AS.Behaviour, {
    constructor: AS.Behaviours.Shark,

    decideAction: function() {
        var self = this;

        // ask underlying class for action
        var baseAction = AS.Behaviour.prototype.decideAction.call(this);
        if (baseAction)
            return baseAction;

        var collidable = ENV.getCollidableObjects(this.agent);
        // get obstacles in direction of agent
        var obstacles = AS.CollisionHelper.getPotentialObstacles(
            this.agent.position,
            this.agent.direction,
            collidable
        );

        var pathBlocked = false;
        $.each(obstacles, function(oi, obstacle) {
            if (obstacle.collision.distance <= self.agent.K.collisionThreshold) {
                pathBlocked = true;
                return false;
            }
        });

        if (pathBlocked)
            return AS.Agent3D.Actions.Rotation.About;
        return AS.Agent3D.Actions.Movement.Forward;
    },
});
