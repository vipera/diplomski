
AS.SimulationStates = {
	Setup: 1,
	Paused: 2,
	Running: 3,
};

AS.Simulation = function() {
	AS.Base.call(this);

	this.agentBehaviours = [
		'SitStill',
		'SimpleDown',
		'Shark',
		'Exploration'
	];

	this.totalTime = 0;
	this.startTime = null;

	this.scenarioData = null;
	this.graphicsHelper = new AS.GraphicsHelper();
	this.menu = new AS.Menu({
		behaviours: this.agentBehaviours,
	});

	this.animationDuration = 1000;
	this.axisHelperShown = true;

	/* HYPERTIMER - setting an interval apparently doesn't work well with THREE.js
	this.hypertimerOptions = {
		rate: 1,
		time: new Date(2015, 10, 1, 14, 00, 00),//new Date(),

		interval: 1000,
		firstTime: 0,
		intervalObj: null,
	}
	this.hypertimer = null;
	*/

	// camera starting position (will look towards 0,0,0)
	this.cameraStartPos = new THREE.Vector3(0, 50, 60);

	// draw menu
	this.menu.updateMenu();
};

AS.Simulation.prototype = AS.inherit(AS.Base, {
	constructor: AS.Simulation,

	get time() {
		if (this.startTime == null)
			return this.totalTime;
		return Date.now() - this.startTime + this.totalTime;
	},

    init: function() {
		var self = this;
        this.state = AS.SimulationStates.Setup;

        AS.Base.prototype.init.call(this);

		// scenario loader dialog gets scenario to load
		AS.DialogHelper.showScenarioLoader(function(event, selected) {
			// load scenario data from server
			$.getJSON("scenario-json.php", {
				scenario_id: selected,
			},
			function(data) {
				if (data['error'] != false) {
					alert(data['error']);
				}
				else {
					self.scenarioData = data;
					AS.Simulation.prototype.setupScene.call(self);
				}
			});
		});
    },

	addAgent: function(agentType, behaviourType, relPos, relDir) {
		var agentObj = AS.stringToFunction(agentType);
		var behaviourObj = AS.stringToFunction(behaviourType);
		var agent = new agentObj(new behaviourObj);

		// give agent some knowledge about where it is
		agent.inform('position', relPos);
		agent.inform('direction', relDir);

		// maximum depth of terrain (submarine mustn't dive any further)
		agent.inform('maxdepth', SIM.scenarioData.maxdepth);
		agent.inform('collisionThreshold', SIM.scenarioData.unitlength);

		// goal function
		agent.inform('goal', function(agent) {
			if (SIM.scenarioData && SIM.scenarioData.goal) {
				var goal = SIM.scenarioData.goal;

				// boxspace goal
				if (goal[0].constructor === Array) {
					if (agent.K.position.x >= goal[0][0] && agent.K.position.x <= goal[1][0] &&
						agent.K.position.y >= goal[0][1] && agent.K.position.y <= goal[1][1] &&
						agent.K.position.z >= goal[0][2] && agent.K.position.z <= goal[1][2]) {
						return true;
					}
				}
				// cell goal
				else if (agent.K.position.x == goal[0] &&
					agent.K.position.y == goal[1] &&
					agent.K.position.z == goal[2]) {
					return true;
				}
			}
			return false;
		});

		var worldPos = AS.SpatialHelper.relativePositionToWorldPosition(relPos);
		var worldDir = AS.SpatialHelper.relativeDirectionToWorldDirection(relDir);

		agent.position.copy(worldPos);
		agent.direction.copy(worldDir);
		agent.updateCameras();

		agent.initKnowledge();

		AS.Log.append(
			'Agent A{0} added'.format(agent.AID),
			'at ({0}) facing ({1}) with behaviour <em>{2}</em>'.format(
				'{0},{1},{2}'.format(relPos.x, relPos.y, relPos.z),
				'{0},{1}'.format(relDir.x, relDir.y),
				behaviourType
			)
		);

		this.scene.add(agent.object);
	},

	removeAgent: function(AID) {
		var agent = ENV.getAgent(AID);
		SIM.scene.remove(agent.object);
		ENV.unregisterAgent(AID);
		AS.Log.append('Agent A{0} removed'.format(AID), 'from scene.');
	},

	changeAgentBehaviour: function(AID, behaviour) {
		var agent = ENV.getAgent(AID);
		agent.behaviour = behaviour;
		behaviour.setAgent(agent);
		AS.Log.append('Agent A{0}\'s behaviour updated'.format(AID), 'to <em>AS.Behaviours.{0}</em>'.format(behaviour.className));
	},

	setupScene: function() {
		var self = this;

		/*this.hypertimer = hypertimer({
			rate: self.hypertimerOptions.rate,
			date: self.hypertimerOptions.date,
		});*/

		this.graphicsHelper.makeSkybox(this.scene, 5000, 'underwater');

		if (this.axisHelperShown)
			this.graphicsHelper.addAxisHelper(this.scene);

		var sceneGeom = this.graphicsHelper.makeSceneGeometry(this.scenarioData);
		var material = new THREE.MeshPhongMaterial({
						color: 0x156289,
						emissive: 0x072534,
						//side: THREE.DoubleSide,
						//shading: THREE.FlatShading,
						transparent: true,
						opacity: 0.8,
						depthTest:true,
			});
		this.sceneMesh = new THREE.Mesh( sceneGeom, material );
		this.scene.add( this.sceneMesh );

		//var subMesh = this.graphicsHelper.makeSubGeometry(this.scenarioData);
		//var sub = new THREE.Mesh(subMesh, material);

		$.each(this.scenarioData.deployment, function(index, depl) {
			// instantiate agents
			self.addAgent(depl.type, depl.behaviour,
				new THREE.Vector3(depl.relposition[0], depl.relposition[1], depl.relposition[2]),
				new THREE.Vector2(depl.reldirection[0], depl.reldirection[1])
			);
			/*
			var agentObj = AS.stringToFunction(depl.type);
			var behaviourObj = AS.stringToFunction(depl.behaviour);
			var agent = new agentObj(new behaviourObj);

			// give agent some knowledge about where it is
			agent.inform('position', new THREE.Vector3(depl.relposition[0],
				depl.relposition[1], depl.relposition[2]));

			agent.inform('direction', new THREE.Vector2(depl.reldirection[0],
				depl.reldirection[1]));

			// maximum depth of terrain (submarine mustn't dive any further)
			agent.inform('maxdepth', depl.maxdepth);

			agent.inform('collisionThreshold', SIM.scenarioData.unitlength);

			// goal function
			agent.inform('goal', function(agent) {
	            if (SIM.scenarioData && SIM.scenarioData.goal) {
	                var goal = SIM.scenarioData.goal;
	                if (agent.position.x == goal.x &&
						agent.position.y == goal.y &&
	                    agent.position.z == goal.z) {
	                    return true;
	                }
	            }
	            return false;
	        });

			agent.position.set(depl.position[0], depl.position[1], depl.position[2]);
			agent.direction.set(depl.direction[0], depl.direction[1], depl.direction[2]);
			agent.updateCameras();

			agent.initKnowledge();

			self.scene.add(agent.object);
			*/
		});
		/*
		var sub = new AS.Submersible(new AS.Behaviours.SimpleDown());
		sub.position.set(10, -10, 10);
		sub.updateCameras();
		this.scene.add(sub.object);

		var sub2 = new AS.Submersible(new AS.Behaviours.SitStill());
		sub2.position.set(10, -10, 30);
		sub2.direction.set(1, 0, 0);
		sub2.updateCameras();
		this.scene.add(sub2.object);*/

		this.camera.position.x = this.cameraStartPos.x;
		this.camera.position.y = this.cameraStartPos.y;
		this.camera.position.z = this.cameraStartPos.z;
		this.camera.lookAt(0, 0, 0);

		this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

		var ambientLight = new THREE.AmbientLight( 0x000000 );
		this.scene.add( ambientLight );

		var lights = [];
			lights[0] = new THREE.PointLight( 0xffffff, 1, 0 );
			lights[1] = new THREE.PointLight( 0xffffff, 1, 0 );
			lights[2] = new THREE.PointLight( 0xffffff, 1, 0 );

			lights[0].position.set( 0, 200, 0 );
			lights[1].position.set( 100, 200, 100 );
			lights[2].position.set( -100, -200, -100 );

			this.scene.add( lights[0] );
			this.scene.add( lights[1] );
			this.scene.add( lights[2] );

		/*var geometry = new THREE.BoxGeometry( 10, 10, 10 );
		var material = new THREE.MeshPhongMaterial({
					color: 0x156289,
					emissive: 0x072534,
					side: THREE.DoubleSide,
					shading: THREE.FlatShading,
					transparent: true,
					opacity: 0.2,
					depthTest:false,
				})
		var cube = new THREE.Mesh( geometry, material );
		this.scene.add( cube );*/

		/*var render = function () {

				requestAnimationFrame( render );

				var time = Date.now() * 0.001;
				//cube.rotation.x += 0.005;
				//cube.rotation.y += 0.005;

				self.renderer.render( self.scene, self.camera );
			};
		render();*/

		this.state = AS.SimulationStates.Paused;
	},

	showAxisHelper: function() {
		this.axisHelperShown = true;
		this.graphicsHelper.addAxisHelper(this.scene);
	},

	hideAxisHelper: function() {
		this.axisHelperShown = false;
		this.graphicsHelper.removeAxisHelper(this.scene);
	},

	isPaused: function() {
		return this.state == AS.SimulationStates.Paused;
	},

	pause: function() {
		this.state = AS.SimulationStates.Paused;

		if (this.startTime) {
			this.totalTime += Date.now() - this.startTime;
			this.startTime = null;
		}

		//this.hypertimer.pause();
		this.menu.updateMenu();
	},

	unpause: function() {
		this.state = AS.SimulationStates.Running;
		this.startTime = Date.now();

		/*
		// start hypertimer for the first time
		if (!this.hypertimerOptions.intervalObj) {
			this.hypertimerOptions.intervalObj = this.hypertimer.setInterval(
				this.tick,
				this.hypertimerOptions.interval,
				this.hypertimerOptions.firstTime
			);
		}
		this.hypertimer.continue();
		*/

		this.menu.updateMenu();
	},

	agentFulfilledGoal: function(agent) {
		AS.Log.append('Agent A{0}'.format(agent.AID), 'fulfilled goal!');
		this.scene.remove(agent.object);
		ENV.unregisterAgent(agent.AID);
		this.menu.updateMenu();
	},

	disableAgentView: function() {
		$.each(this.additionalRenderers, function(ir, renderer) {
			renderer.forceContextLoss();
			renderer.context = null;
			renderer.domElement = null;
			renderer = null;
		});
		this.additionalRenderers = {};
	},

	restart: function() {
		this.state = AS.SimulationStates.Setup;
		ENV.unregisterAllAgents();

		AS.Log.append('Simulation restarted', '');

		this.reload(); // reload scene

		/*this.hypertimer.clearInterval(this.hypertimerOptions.intervalObj);
		this.hypertimerOptions.intervalObj = null;
		this.hypertimer = null;*/

		this.setupScene();
		this.menu.updateMenu();
		this.drawAdditional();
	},

	tick: function() {
		ENV.tickAgents();
	},

	update: function() {
		if (this.controls != undefined)
			this.controls.update();

		this.handleKeyPresses();

		// from this point down, don't update
		if (this.state != AS.SimulationStates.Running)
			return;

		this.tick();

		ENV.callOnAgents(function(agent) { agent.updateAnimation(); });
	},

	handleKeyPresses: function() {
		if (AS.InputHelper.isKeyDown(90)) // Z
			this.resetCamera();
		if (AS.InputHelper.isKeyDown(80)) // P
			$('#menuitem-pausesimulation').trigger('click');
	},

	resetCamera: function() {
		this.camera.position.x = this.cameraStartPos.x;
		this.camera.position.y = this.cameraStartPos.y;
		this.camera.position.z = this.cameraStartPos.z;
		this.camera.lookAt(0, 0, 0);

		if (this.controls) {
			this.controls.center.set(0,0,0);
        	this.controls.update();
		}
    },

	drawAdditional: function() {
		var self = this;

		// agent's cameras need to be displayed
		if (this.menu.agentView) {
			var cameras = ENV.getAgentCameras(this.menu.viewingAgent);

			var createRenderers = false;
			if (Object.keys(this.additionalRenderers).length < cameras.length)
				createRenderers = true;

			$.each(cameras, function(ci, camera) {
				var renderer = null;
				if (createRenderers) {
					var newRenderer = new THREE.WebGLRenderer({ antialias: self.antialias });
					newRenderer.setClearColor(self.clearColor, 1);
					newRenderer.setSize(self.agentViewCameraSize[0], self.agentViewCameraSize[1]);
					newRenderer.domElement.id = "agentview-canvas-" + ci;

					self.additionalRenderers[camera.id] = newRenderer;
					renderer = newRenderer;

					$('#agentview').append('<div class="camera-' + camera.id + '"></div>');
					$('#agentview div').last().append('<span class="title">' + camera.name + '</span>');
					$('#agentview div').last().append('<div class="canvas"></div>');
					$('#agentview div.canvas').last().append(renderer.domElement);
				}
				else {
					renderer = self.additionalRenderers[camera.id]
				}

				renderer.render(self.scene, camera.camera);
			});
		}
	},
});

var SIM;
var ENV;
window.addEventListener("load", function() {
	ENV = new AS.Environment();
	SIM = new AS.Simulation();

    //SIM.preInit();
    SIM.init();
}, false);
