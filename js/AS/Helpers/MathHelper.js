AS.MathHelper = function() {};

AS.MathHelper.rotateAroundObjectAxis = function(object, axis, radians) {
    var rotObjectMatrix = new THREE.Matrix4();
    rotObjectMatrix.makeRotationAxis(axis.normalize(), radians);

    object.matrix.multiply(rotObjectMatrix); // post-multiply

    object.rotation.setFromRotationMatrix(object.matrix);
};

AS.MathHelper.rotateAroundWorldAxis = function(object, axis, radians) {
    var rotWorldMatrix = new THREE.Matrix4();
    rotWorldMatrix.makeRotationAxis(axis.normalize(), radians);

    rotWorldMatrix.multiply(object.matrix); // pre-multiply
    object.matrix = rotWorldMatrix;

    object.rotation.setFromRotationMatrix(object.matrix);
};

/**
 * Euclidian distance between two 3-vectors
 */
AS.MathHelper.distance = function(vec1, vec2) {
    var dx = vec1.x - vec2.x;
    var dy = vec1.y - vec2.y;
    var dz = vec1.z - vec2.z;

    return Math.sqrt(dx*dx + dy*dy + dz*dz);
};

// http://stackoverflow.com/questions/9960908/permutations-in-javascript
AS.MathHelper.permutator = function(inputArr) {
    var results = [];

    function permute(arr, memo) {
        var cur, memo = memo || [];

        for (var i = 0; i < arr.length; i++) {
            cur = arr.splice(i, 1);
            if (arr.length === 0) {
                results.push(memo.concat(cur));
            }
            permute(arr.slice(), memo.concat(cur));
            arr.splice(i, 0, cur[0]);
        }

        return results;
    }

    return permute(inputArr);
};
