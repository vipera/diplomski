<?php
require './global.php';

function get_box_coords($scene, $xc, $yc, $zc) {
    // starting position for each box is aligned with x-z below the y axis
    // these are THREEJS coordinates
    $pos = array(
        'x' => $scene['unitlength'] / 2,
        'y' => -$scene['unitlength'] / 2,
        'z' => $scene['unitlength'] / 2,
    );

    // calculate the "central" box to evenly distrute the scene around the y axis
    $centerbox_x = floor($scene['xspan'] / 2.);
    $centerbox_y = floor($scene['yspan'] / 2.);

    // offset (in boxes) to move this box left or right
    $moveto_x = $xc - $centerbox_x;
    $moveto_y = $yc - $centerbox_y;
    $moveto_z = -$zc;

    // update THREEJS coords (z axis is actually y)
    $pos['x'] += $moveto_x * $scene['unitlength'];
    $pos['y'] += $moveto_z * $scene['unitlength'];
    $pos['z'] += $moveto_y * $scene['unitlength'];

    return $pos;
}

function update_scenario_info(&$scenario, $scene, $cell, $tc, $xc, $yc, $zc) {
    $pos = get_box_coords($scene, $xc, $yc, $zc);

    $scenario['scene'][$tc]['x'] = $pos['x'];
    $scenario['scene'][$tc]['y'] = $pos['z'];
    $scenario['scene'][$tc]['z'] = $pos['y'];
    $scenario['scene'][$tc]['solid'] = $cell['block'] == 'yes';
}

function calculate_culling(&$scenario, $xml_scene) {
    if ($xml_scene == null)
        return;

    $data = array(
        'scenario' => $scenario,
        'scene' => $scenario['scene'],

        'xspan' => (int)$xml_scene['xspan'],
        'yspan' => (int)$xml_scene['yspan'],
        'zspan' => (int)$xml_scene['zspan'],
        'xyspan' => (int)$xml_scene['xspan'] * (int)$xml_scene['yspan'],
    );

    $up = function($n, $data) {
        if (!$data['scene'][$n]['solid'] ||
            $n < $data['xyspan'] ||
            !$data['scene'][$n - $data['xyspan']]['solid'])
            return false;
        return true;
    };

    $down = function($n, $data) {
        if (!$data['scene'][$n]['solid'] ||
            $n >= ($data['zspan'] - 1) * $data['xyspan'] ||
            !$data['scene'][$n + $data['xyspan']]['solid'])
            return false;
        return true;
    };

    $left = function($n, $data) {
        if (!$data['scene'][$n]['solid'] ||
            $n % $data['xyspan'] < $data['yspan'] ||
            !$data['scene'][$n - $data['yspan']]['solid'])
            return false;
        return true;
    };

    $right = function($n, $data) {
        if (!$data['scene'][$n]['solid'] ||
            ($n + $data['yspan']) % $data['xyspan'] < $data['yspan'] ||
            !$data['scene'][$n + $data['yspan']]['solid'])
            return false;
        return true;
    };

    $front = function($n, $data) {
        if (!$data['scene'][$n]['solid'] ||
            $n % $data['yspan'] == 0 ||
            !$data['scene'][$n - 1]['solid'])
            return false;
        return true;
    };
    $back = function($n, $data) {
        if (!$data['scene'][$n]['solid'] ||
            ($n + 1) % $data['yspan'] == 0 ||
            !$data['scene'][$n + 1]['solid'])
            return false;
        return true;
    };

    $fnlist = array(
        'right' => $right,
        'left'  => $left,
        'back'  => $back,
        'front' => $front,
        'up'    => $up,
        'down'  => $down,
    );

    for ($n = 0; $n < count($data['scene']); $n++) {
        $scenario['scene'][$n]['culling'] = array();

        foreach ($fnlist as $direction => $fn) {
            $scenario['scene'][$n]['culling'][] = $fn($n, $data);
        }
    }
}

function string_to_vector($vector) {
    $parts = array_map('intval', explode(',', strval($vector)));
    return $parts;
}

function direction_vector($vector) {
    $vector = string_to_vector($vector);
    return array(
        $vector[0],
        0,
        $vector[1]
    );
}

function map_vector(&$scenario, $xml_scene, $vector) {
    $vector = string_to_vector($vector);

    $centerbox_x = floor($xml_scene['xspan'] / 2.);
    $centerbox_y = floor($xml_scene['yspan'] / 2.);

    $moveto_x = $vector[0] - $centerbox_x;
    $moveto_y = $vector[1] - $centerbox_y;

    $newvector = array(
        $moveto_x * $xml_scene['unitlength'] + $xml_scene['unitlength'] / 2,
        count($vector) > 2 ? $vector[2] * -$xml_scene['unitlength'] - $xml_scene['unitlength'] / 2 : 0,
        $moveto_y * $xml_scene['unitlength'] + $xml_scene['unitlength'] / 2
    );

    if (count($vector) <= 2)
        $newvector = array_slice($newvector, 0, 2);
    return $newvector;
}

$dir = CWD . '/scenarios';
$scenario_id = isset($_GET['scenario_id']) ? $_GET['scenario_id'] : null;

$file = $dir . '/' . $scenario_id . '.xml';

$scenario = array('error' => false);
$culling_scene = null;

if (is_readable($file))
{
    // validate XML against XSD
    $testdoc = new DOMDocument();
    $testdoc->load($file);
    if (!$testdoc->schemaValidate(CWD . '/scenarios/agent-setup.xsd'))
    {
        $scenario['error'] = 'Unable to load scenario: does not validate.';
    }
    else
    {
        $agent_setup = simplexml_load_file($file);
        $scene = $agent_setup->scene;
        $culling_scene = $scene;

        $scenario['unitlength'] = (int)$scene['unitlength'];

        $scenario['deployment'] = array();

        if ($scene['type'] == 'box') {
            $xspan = $scene['xspan'];
            $yspan = $scene['yspan'];
            $zspan = $scene['zspan'];

            $scenario['maxdepth'] = intval($zspan) - 1;
            $scenario['center'] = array(
                floor($xspan / 2.),
                floor($yspan / 2.),
                0
            );

            // set goal
            $obj = $agent_setup->objective;
            if ($obj->cell) {
                $pos = array_map('intval', explode(',', $obj->cell['position']));
                $scenario['goal'] = $pos;
            }
            elseif ($obj->boxspace) {
                $from = array_map('intval', explode(',', $obj->boxspace['from']));
                $to = array_map('intval', explode(',', $obj->boxspace['to']));
                $scenario['goal'] = array(
                    $from,
                    $to
                );
            }

            // iterate through box layers (down Z axis)
            $tc = $xc = $yc = $zc = 0;
            foreach ($scene->layer as $layer)
            {
                foreach ($layer->row as $row)
                {
                    foreach ($row->cell as $cell)
                    {
                        if ($cell['origin'] == 'yes') {
                            // mark origin
                            $scenario['origin'] = array($yc, $xc, $zc);
                        }
                        if ($cell['repeat'] == 'yes')
                        {
                            // this cell type is repeated through the rest of
                            // the row
                            for (; $yc < $yspan; $yc++)
                            {
                                update_scenario_info($scenario, $scene, $cell,
                                    $tc, $xc, $yc, $zc);
                                $tc++;
                            }
                        }
                        else
                        {
                            update_scenario_info($scenario, $scene, $cell, $tc,
                                $xc, $yc, $zc);
                            $tc++;
                        }
                        if (++$yc >= $yspan) $yc = 0;
                    }
                    if (++$xc >= $xspan) $xc = 0;
                }
                if (++$zc >= $zspan) $zc = 0;
            }
        }

        foreach ($agent_setup->deployment->agents->agent as $agent) {
            $agent_data = array();

            $agent_data['name'] = strval($agent['name']);

            $agent_data['type'] = strval($agent['type']);
            $agent_data['behaviour'] = strval($agent['behaviour']);

            $agent_data['position'] = map_vector($scenario, $scene, $agent['startposition']);
            $agent_data['relposition'] = array_map('intval', explode(',', $agent['startposition']));
            $agent_data['direction'] = direction_vector($agent['startdirection']);
            $agent_data['reldirection'] = array_map('intval', explode(',', $agent['startdirection']));
            $scenario['deployment'][] = $agent_data;
        }
    }
}
else
{
    // error - could not find scenario
    $scenario['error'] = 'Unable to load scenario: not found.';
}

calculate_culling($scenario, $culling_scene);
print(json_encode($scenario));
//print_r($scenario);

?>
